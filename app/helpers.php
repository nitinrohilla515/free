<?php 
use Carbon\Carbon;
use App\Models\User;
use Intervention\Image\Facades\Image;
use Spatie\PdfToImage\Pdf;
use App\Models\UserNotification;
use App\Models\Rider;
use App\Models\RiderNotification;
use App\Models\AcceptParcel;
use App\Models\RiderRating;
use App\Models\ParcelLog;
use App\Models\ParcelRequest;
use App\Models\UserRating;
use App\Models\Payment;
use App\Models\CancelledParcel;



use App\Models\ApiLog;
use Illuminate\Support\Facades\Log;















define('PROJECT_START_DATE', '2023-12-01');
define('TIME_FORMAT', 'Y-m-d H:i:s');
define('ADMIN', 1);
define('SUBADMIN', 2);
// define('firebasecredentails', base_path('firebase-credentials.json'));


function defaultImage() {
    return url('/') . "/assets/img/no-image.jpg";
}

function version() {
    return "2.8.4";
}

function base64Encode($value) {
    return base64_encode(base64_encode($value));
}

function base64Decode($value) {
    return base64_decode(base64_decode($value));
}

function ajaxSuccessResponse($status, $message = '', $data = []) {
    return [
        'success' => $success,
        'message' => $message,
        'data' => $data,
    ];
}
function ajaxResponse($status, $message = '', $data = []) {
    return [
        'data' => $data,
        'status' => $status,
        'message' => $message
    ];
}

function email_template($email_slug) {
    return App\Models\EmailTemplate::where('slug', $email_slug)->first();
}

function replace_content($mail, $emaildata) {

    if (isset($emaildata)) {
        foreach ($emaildata as $k => $v) {
            if (is_null($v) || $v == '') {
                $v = '';
            }
            $mail = str_replace('{{' . $k . '}}', $v, $mail);
        }
    }
    return $mail;
}

function defaultTimeZone() {

    return "Asia/kolkata";
}


function convertTimezoneToUTC($date) {
  
        $timezone = defaultTimeZone();
    
    return parseDisplayDateTime(Carbon::createFromFormat(TIME_FORMAT, $date, $timezone)->setTimezone('UTC'));
}

function convertUtcToTimezone($date) {
  
        $timezone = defaultTimeZone();
    
    return parseDisplayDateTime(Carbon::createFromFormat(TIME_FORMAT, $date, 'UTC')->setTimezone($timezone));
    
}

function dbTimezoneToUTC($date) {
   
        $timezone = defaultTimeZone();

    return Carbon::createFromFormat(TIME_FORMAT, $date, $timezone)->setTimezone('UTC');
}

function dbUtcToTimezone($date) {
 
        $timezone = defaultTimeZone();
  
    return Carbon::createFromFormat(TIME_FORMAT, $date, 'UTC')->setTimezone($timezone);
}

function parseDisplayDate($date) {
    return date('m/d/Y', strtotime($date));
}

function parseDisplayDateTime($date) {
    return date('m/d/Y h:i A', strtotime($date));
}

function parseDisplayDateNew($date) {
    return date('d M, Y', strtotime($date));
}

function parseDisplayTime($date) {
    return date('h:i A', strtotime($date));
}

function isDeleted() {
    if (isset(request()->deleted)) {
        return 'Deleted';
    }
    return '';
}

function generateOTP() {
    if (env('APP_ENV') != 'production') {
        $otp_code = 1234;
    } else {
        $otp_code = str_pad(rand(1, 9999), 4, '0', STR_PAD_LEFT);
    }

    return $otp_code;
}

function findCountryId($phoneCode){
  return  App\Models\Country::where('phonecode', $phoneCode)->first();
}


function getStatusText($status){
    switch ($status) {
        case 0:
            $text = 'Pending';
            break;
        case 1:
            $text = 'Order accepted';
            break;
        case 2:
            $text = 'Rider assigned';
            break;
        case 3:
            $text = 'Item picked';
            break;
        case 4:
                $text = 'On the Way';
                break;    
        case 5:
                    $text = 'Reaching to dropping location';
                    break;     
       case 6:
              $text = 'Delivered';
              break;                 

        default:
            // Handle default case or leave it as is
            break;
    }
    return $text;
}


function getStatusBtnColor($status){
    switch ($status) {
        case 0:
            $text = 'Yellow';
            break;
        case 1:
            $text = 'Order accepted';
            break;
        case 2:
            $text = 'Rider assigned';
            break;
        case 3:
            $text = 'Item picked';
            break;
        case 4:
                $text = 'On the Way';
                break;    
        case 5:
                    $text = 'Reaching to dropping location';
                    break;     
       case 6:
              $text = 'Delivered';
              break;                 

        default:
            // Handle default case or leave it as is
            break;
    }
    return $text;
}


function statusTextArray(){
    return ['Order accepted', 'Rider assigned', 'Item picked', 'On the way', 'Reaching to dropping location', 'Delivered'];
}

function parcelStatusArray() {
    $statuses = array();

    for ($i = 0; $i <= 6; $i++) {
        $statuses[] = getStatusText($i);
    }

    return $statuses;
}

function getJwtSecretkey(){
    return env('JWT_SECRET');
}


function saveFile($file, $path){
    $extension = $file->getClientOriginalExtension();
    $filename = hash('sha256', time() . $file->getClientOriginalName()) . '.' . $extension;

    // Use Intervention Image to resize
    // Image::make($file)->resize(800, null, function ($constraint) {
    //     $constraint->aspectRatio();
    // })->save(storage_path('app/public/{$path}' . $filename));
    if (in_array(strtolower($extension), ['jpg', 'jpeg', 'png', 'gif'])) {
        // Use Intervention Image to resize (if it's an image)
        Image::make($file)->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/uploads/{$path}/".$filename));
    } elseif (strtolower($extension) === 'pdf') {
        // Convert PDF to image using spatie/pdf-to-image
        // $pdf = new Pdf($file->getRealPath());
        // $file->storeAs(storage_path("app/public/uploads/{$path}/", $filename));
        // $file->storeAs(
        //     storage_path("app/public/uploads/{$path}/"), // Path
        //     $filename // Filename
        // );   
        // $file->storeAs(
        //     'public', // Storage disk
        //     "uploads/{$path}/", // Path
        //     $filename // Filename
        // );
        $file->storeAs(
            'public', // Storage disk
            "uploads/{$path}/{$filename}" // Path and filename
        );
     } else {
        // For non-image and non-PDF files, simply move them to the specified path
        $file->storeAs(storage_path("app/public/uploads/{$path}/", $filename));
    }

    return "{$path}/{$filename}";
}



 function createNotification($userId, $type, $title, $body, $data = [])
{
    return UserNotification::create([
        'user_id' => $userId,
        'type' => $type,
        'title' => $title,
        'body' => $body,
        'data' => $data, 
    ]);
}

function createRiderNotification($riderId, $type, $title, $body, $data = [])
{
    return RiderNotification::create([
        'rider_id' => $riderId,
        'type' => $type,
        'title' => $title,
        'body' => $body,
        'data' => $data, 
    ]);
}

 function getUserNotifications($userId)
{
    return UserNotification::where('user_id', $userId)->latest()->get();
}

function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit) {
    $radius = ($unit == 'km') ? 6371 : 3959; // Radius of the Earth in kilometers or miles

    $lat1 = deg2rad($lat1);
    $lon1 = deg2rad($lon1);
    $lat2 = deg2rad($lat2);
    $lon2 = deg2rad($lon2);

    $deltaLat = $lat2 - $lat1;
    $deltaLon = $lon2 - $lon1;

    $a = sin($deltaLat / 2) * sin($deltaLat / 2) + cos($lat1) * cos($lat2) * sin($deltaLon / 2) * sin($deltaLon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

    $distance = $radius * $c;

    return $distance;
}

// function calculateDistance($origin, $destination) {
      

//     $url = "https://maps.googleapis.com/maps/api/distancematrix/json";
//     $params = [
//         'origins' => $origin,
//         'destinations' => $destination,
//         'key' => env('GOOGLE_MAP_KEY'),
//     ];

//     $url .= '?' . http_build_query($params);

//     $response = file_get_contents($url);
//     $data = json_decode($response, true);
//     // Build the Google Maps Distance Matrix API URL
//     // print_r($data);
//       return $data;

//     if ($data['status'] === 'OK') {
//         $distance = $data['rows'][0]['elements'][0]['distance']['value'];
//         return $distance;
//     }

//     return null;
// }

 function checkRiders($pickupLatitude, $pickupLongitude){
    $radius = env('MAX_DELIVERY_DISTANCE');
    $minRidersToFind = 2; 

$nearbyRiders = [];
$foundRidersCount = 0;

$riders = Rider::where('status', '1')->whereNotNull('latitude')->whereNotNull('longitude')->get();

foreach ($riders as $rider) {
    $riderLatitude = $rider->latitude;
    $riderLongitude = $rider->longitude;

    $distance = calculateDistance($pickupLatitude, $pickupLongitude, $riderLatitude, $riderLongitude, 'km');

    if ($distance <= $radius) {
        // Rider is within the 10 km radius
        $nearbyRiders[] = $rider;
        $foundRidersCount++;

        // Break the loop if the minimum number of riders is reached
        if ($foundRidersCount >= $minRidersToFind) {
            break;
        }

    }
 }
 return $foundRidersCount;
}


function calculateAmount($parcelType, $distance, $itemWeight) {
    $ratePerKm = env('RATE_PER_KM'); 

    $amount = $ratePerKm * $distance;

    return $amount;
}

function getParcelStatus($parcelId){
   $acceptParcel = AcceptParcel::where('parcel_request_id', $parcelId)->first();
   $currentStatus = isset($acceptParcel) ? $acceptParcel->status : '0';
   return $currentStatus;
}


function calRiderRating($riderId) {
    $averageRating = RiderRating::where('rider_id', $riderId)
        ->avg('rating');

    return $averageRating;
}

function calUserRating($userId) {
    $averageRating = UserRating::where('user_id', $userId)
        ->avg('rating');

    return $averageRating;
}



function successResponse($message, $data){
return [
    'success' => true,
    'message' =>  $message,
    'data' => $data,
];
}



function falseResponse($message, $data, $r = null){
//    return  request()->path();
    ApiLog::create([
        'endpoint' => request()->path(), 
        'method' => request()->method(),
        'request' => request()->all(), 
        'message' => $message,
        'response' => json_encode($data),
    ]);

    return [
        'success' => false,
        'message' =>  $message,
        'data' => null,
    ];
}



function journeyStatusText($status){
    switch ($status) {
        case 1:
            $text = 'Parcel delivery started';
            $body = 'Rider started your parcel delivery';
            break;
        case 2:
            $text = 'Reached pickup point';
            $body = 'Rider reached pickup point';

            break;
        case 3:
            $text = 'Parcel picked';
            $body = 'Your parcel has been picked by rider';

            break;
        case 4:
            $text = 'Start delivering';
            $body = 'Rider has started delivering';

            break;
        case 5:
                $text = 'Reached drop location';
                $body = 'Rider has reached drop location';

                break;    
        case 6:
                    $text = 'Parcel Delivered';
                    $body = 'Your parcel has been delivered';

                    break;                    

        default:
            // Handle default case or leave it as is
            break;
    }
    return ['title' => $text, 'body' => $body];
}


function filterDateParameters($r) {
    $params['filterStartDate'] = $rStart = $r->filterStartDate ? $r->filterStartDate : PROJECT_START_DATE;
    $params['filterEndDate'] = $rEnd = $r->filterEndDate ? $r->filterEndDate : Carbon::now()->format('Y-m-d');
    $params['label'] = $label = $r->label ? $r->label : 'Lifetime';
    $params['filterStatus'] = $r->filterStatus ? $r->filterStatus : '';


    if ($label == "Today" || $label == "Yesterday" || $label == "Last 7 Days" || $label == "Last 30 Days" || $label == "Custom Range") {
        $start = dbTimezoneToUTC($rStart . " 00:00:00");
        $end = dbTimezoneToUTC($rEnd . " 23:59:59");
    } else {
        $start = $rStart . " 00:00:00";
        $end = $rEnd . " 23:59:59";
    }


    return compact('params', 'start', 'end');
}

function dateFilter($query, $r, $start, $end) {
    if (isset($r->filterStartDate) && isset($r->filterEndDate) && $r->filterStartDate != '' && $r->filterEndDate != '') {
        $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
    }
    return $query;
}

function createParcelData($parcelLogId) {
    // Find the ParcelLog record
    $parcelLog = ParcelLog::find($parcelLogId);
    $parcelLog->status = true;
    $parcelLog->save();
    Log::channel('version1')->info('fetched parcelo log ', ['parcelLog' => $parcelLog, 'payment method' => $parcelLog->payment_method]);


    // Create a new ParcelRequest record with the same values as ParcelLog
    $data = [
        'pickup_user_id' => $parcelLog->pickup_user_id,
        'receiver_user_id' => $parcelLog->receiver_user_id,
        'display_name' => $parcelLog->display_name,
        'parcel_type' => $parcelLog->parcel_type,
        'parcel_worth' => $parcelLog->parcel_worth,
        'parcel_details' => $parcelLog->parcel_details,
        'item_weight' => $parcelLog->item_weight,
        'coordinate_data' => $parcelLog->coordinate_data,
        'submit_by_user' => $parcelLog->submit_by_user,
        'amount' => $parcelLog->amount,
        'distance' => $parcelLog->distance,
        'payment_method' => $parcelLog->payment_method,
        ];
    
    $parcelRequest = ParcelRequest::create($data);
    // $parcelRequest = new ParcelRequest;

    // $parcelRequest->pickup_user_id = $parcelLog->pickup_user_id;
    // $parcelRequest->receiver_user_id = $parcelLog->receiver_user_id;
    // $parcelRequest->display_name = $parcelLog->display_name;
    // $parcelRequest->parcel_type = $parcelLog->parcel_type;
    // $parcelRequest->parcel_worth = $parcelLog->parcel_worth;
    // $parcelRequest->parcel_details = $parcelLog->parcel_details;
    // $parcelRequest->item_weight = $parcelLog->item_weight;
    // $parcelRequest->coordinate_data = $parcelLog->coordinate_data;

    // $parcelRequest->submit_by_user = $parcelLog->submit_by_user;
    // $parcelRequest->amount = $parcelLog->amount;
    // $parcelRequest->distance = $parcelLog->distance;
    // // $parcelRequest->fill($parcelLog->toArray());
    // $parcelRequest->save();

    return $parcelRequest;
}

function verifyTransaction($refrence){
     
  $curl = curl_init();
  
  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.paystack.co/transaction/verify/".$refrence,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Authorization: Bearer sk_test_8b030e9fb189b9cd1a3c920c1d88b3a8419d06a0",
      "Cache-Control: no-cache",
    ),
  ));
  
  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  
  if ($err) {
   return $err;
  } else {
    return $response;
  }

}

function updateRefund($data){

 $payment = Payment::where('reference', $data['transaction_reference'])
     ->first();

     Log::channel('version1')->error("upadte refund payment:".$payment);


if ($payment) {
$cancelledParcel = CancelledParcel::where('parcel_request_id', $payment->parcel_id)->first();
Log::channel('version1')->error("cancelled parcel:".$cancelledParcel);


if ($cancelledParcel) {
$cancelledParcel->refund_status = $data['status'];
$cancelledParcel->save();
return true;
}
return false;
}
 }





