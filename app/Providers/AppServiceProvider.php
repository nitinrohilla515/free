<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Models\OrderJourney;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('valid_order_journey_status', function ($attribute, $value, $parameters, $validator) {
            // Check if the specified status is already present or less than the current value
            $parcelId = $validator->getData()['parcel_id'];
            $status = $value;

            $existingStatus = OrderJourney::where('parcel_id', $parcelId)
                ->where('journey_status', '>=', $status)
                ->exists();

            return !$existingStatus;
        });
        //
    }
}
