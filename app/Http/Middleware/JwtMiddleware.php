<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth as TymonJWTAuth;
use Exception;
use Firebase\JWT\JWT as FirebaseJWT;
use stdClass;
use Firebase\JWT\Key;
use UnexpectedValueException;
use App\Services\AuthService;
use Illuminate\Support\Facades\Log;
use App\Models\User;








class JwtMiddleware
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    { 
        // $token = $request->header('Authorization');
        // $platform = $request->header('platform'); 
        
    //     if (!$platform || !in_array($platform, ['Android', 'ios'])) {
    //         return response()->json(['success' => false, 'message' => 'Invalid platform', 'data' => []], 401);
    //     }
       $token =  $request->bearerToken();
       if(!$token){
       return response()->json(falseResponse('Invalid token', []), 401);
       }

    //    if(!$token){
    //     return response()->json(['success' => false, 'message' => 'Invalid token', 'data' => []], 401);

    //    }
       

        // print_r($token);
        try {

            $key = env('JWT_SECRET');

            // $decoded = FirebaseJWT::decode($token, $key, 'HS256');
            // $decoded = FirebaseJWT::decode($token, $key, array('HS256'));
            $decoded = FirebaseJWT::decode($token, new Key($key, 'HS256'));
            if (!property_exists($decoded, 'user_id')) {
               
                return response()->json(falseResponse('user_id not present in token', []), 401);

            }
            // $userExists = User::where('id', $decoded->user_id)->exists();
            $user = User::find($decoded->user_id);


if (!$user) {
  
    return response()->json(falseResponse('user_id does not exist in our record', []), 401);

}



// Check device_id for single device use
// if (!property_exists($decoded, 'device_id') || $user->device_id !== $decoded->device_id) {
//     return response()->json(falseResponse('Invalid device_id', []), 401);
// }





            $firebaseId = $decoded->firebase_id; 
            $verifyUser = $this->authService->verifyUser($firebaseId);
            $request->merge(['user_id' => $decoded->user_id]);
            $request->merge(['appType' => 'User']);


            if ($verifyUser) {
                return $next($request);
            } else {
                return response()->json(['success' => false, 'message' => 'Invalid token', 'data' => []], 401);
            }
           
        } catch (Exception $e) {
            $errorResponse = [
                'success' => false,
                'message' => 'Token error',
                'data' => ['error' => $e->getMessage()],
            ];
    
            if ($e instanceof \Firebase\JWT\BeforeValidException) {
                $errorResponse['message'] = 'Token is Invalid';
            } elseif ($e instanceof \Firebase\JWT\ExpiredException) {
                $errorResponse['message'] = 'Token is Expired';
            } elseif ($e instanceof \Firebase\JWT\SignatureInvalidException) {
                $errorResponse['message'] = 'Token is Invalid';
            }
    
            // Log the error
            Log::channel('version1')->error($e);
    
            return response()->json($errorResponse, 401);
        }
    
      
                // return $next($request);
    
    }
}
