<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;


class SubmitRating extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

  

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            // 'parcel_id' => 'required|exists:accept_parcels,parcel_request_id',
            'parcel_id' => [
                'required',
                'exists:parcel_requests,id',
                Rule::unique('rider_ratings', 'parcel_id')
                    ->whereNull('deleted_at'),
            ],
            'rating' => 'required|in:1,2,3,4,5'



        ];
    }

    public function messages()
    {
        return [
            'parcel_id.unique' => 'Rating has already been submitted.',
        ];
    }



    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for SubmitRating', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' =>  $firstError,
        //     'data' => [],
        // ], 422));
        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));

    }
}
