<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class CancelParcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'parcel_request_id' => [
                'required',
                'exists:parcel_requests,id',
                Rule::unique('cancelled_parcels', 'parcel_request_id')
                ->whereNull('deleted_at'),
                function ($attribute, $value, $fail) {
                    $parcel = \App\Models\AcceptParcel::where('parcel_request_id', $value)->first();
                if (!$parcel) {
                    return;
                }

                    if ($parcel && $parcel->status >= '0' && $parcel->status <= '2') {
                        return;
                    }
                    $fail("Parcel can't be canceled because the parcel is picked by the rider.");
                },
            ],
            'reason' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'parcel_request_id.unique' => 'Parcel has already been cancelled.',
        ];
    }
    
   
   
    
    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for AcceptParcelRequest', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' =>  $firstError,
        //     'data' => $validator->errors(),
        // ], 422));
        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));

    }
}
