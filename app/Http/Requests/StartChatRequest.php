<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;

class StartChatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
            'parcel_id' => 'required|exists:parcel_requests,id',

        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for Start Chat', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

    

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' => $firstError,
        //     'data' => $validator->errors(),
        // ], 422));
        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));

    }
}
