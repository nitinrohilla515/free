<?php

namespace App\Http\Requests\Rider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use App\Models\Rider;



class StoreProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $id = $this->input('user_id');
        $rider = Rider::with('profile')->find($id);
        return [
            'name' => 'required',
            'age' => 'required|numeric',
            // 'email' => 'required|email|unique:riders,email',
            'email' => [
                $rider->email ? 'nullable' : 'required',
                'email',
                Rule::unique('riders', 'email')->ignore($id)
                // Rule::unique('users', 'email')
            ],
            'address' => 'required',
            // 'profile_photo' => [Rule::requiredIf($rider->profile()  == null),'required|mimes:jpeg,png,jpg|max:2048'],
            // 'driving_license_no' => 'required',
            // 'driving_license_file' => [Rule::requiredIf($id==null),'required|mimes:jpeg,png,jpg,pdf|max:2048'],
            // 'vehicle_registration_no' => 'required',
            // 'vehicle_registration_file' => [Rule::requiredIf($id==null),'required|mimes:jpeg,png,jpg,pdf|max:2048'],
            'profile_photo' => [
                $rider->profile && $rider->profile->profile_photo ? 'nullable' : 'required',
                'mimes:jpeg,png,jpg|max:2048'
            ],
            'driving_license_no' => 'required',
            'driving_license_file' => [
                $rider->profile && $rider->profile->driving_license_file ? 'nullable' : 'required',
                'mimes:jpeg,png,jpg,pdf|max:2048'
            ],
            'vehicle_registration_no' => 'required',
            'vehicle_registration_file' => [
                $rider->profile && $rider->profile->vehicle_registration_file ? 'nullable' : 'required',
                'mimes:jpeg,png,jpg,pdf|max:2048'
            ],



        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for SignupSignin', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
            "email" => $this->input('email'),
            "profile_photo" => $this->input('profile_photo'),
            "driving_license_file" => $this->input('driving_license_file'),
            "vehicle_registration_file" => $this->input('vehicle_registration_file'),

        ]);

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' =>  $firstError,
        //     'data' => null,
        // ], 422));
        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));
    }
}
