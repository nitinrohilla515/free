<?php

namespace App\Http\Requests\Rider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class InitiateTransfer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'bank_id' => 'required|exists:rider_bank_accounts,id',
            'amount' => 'required|numeric',
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for Initiate Transfer', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' => $firstError,
        //     'data' => [],
        // ], 422));

        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));

    }
}
