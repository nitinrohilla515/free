<?php

namespace App\Http\Requests\Rider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;




class SubmitRating extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
   
        public function rules()
        {
            // Validator::extend('user_exists_for_parcel', function ($attribute, $value, $parameters, $validator) {
               
            //         $exists = \DB::table('parcel_requests')
            //         ->where('id', $parameters[0])
            //         ->where('submit_by_user', $value)
            //         ->exists();
            
            //     return $exists;
            // });
                        
            // return [
            //     'parcel_id' => 'required|exists:accept_parcels,parcel_request_id',
            //     'user_id' => 'required|exists:users,id|user_exists_for_parcel:' . request('parcel_id'),
            // ];
            return [
                // 'parcel_id' => 'required|exists:accept_parcels,parcel_request_id',
                'parcel_id' => [
                    'required',
                    'exists:parcel_requests,id',
                    Rule::unique('user_ratings', 'parcel_id')
                        ->whereNull('deleted_at'),
                ],
                'rating' => 'required|in:1,2,3,4,5'
    
    
            ];
        }

        public function messages()
        {
            return [
                'parcel_id.unique' => 'Rating has already been submitted.',
            ];
        }

        protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for SubmitRating', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' =>  $firstError,
            'data' => null,
        ], 422));
    }
    
}
