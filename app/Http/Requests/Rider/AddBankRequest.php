<?php

namespace App\Http\Requests\Rider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;



class AddBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'type' => 'required|string|in:nuban,mobile_money,ghipss',
            // 'account_number' => 'required|string',
            'account_number' => [
                'required',
                Rule::unique('rider_bank_accounts')->where(function ($query) {
                    return $query->where('rider_id', request()->user_id);
                }),
            ],
            'bank_code' => 'required|string',
            'currency' => 'required|string',
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for AcceptParcelRequest', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' => $firstError,
        //     'data' => [],
        // ], 422));

        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));

    }
}
