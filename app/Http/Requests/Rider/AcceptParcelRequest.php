<?php

namespace App\Http\Requests\Rider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class AcceptParcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'parcel_request_id' => [
                'required',
                'exists:parcel_requests,id',
                Rule::unique('accept_parcels', 'parcel_request_id')
                    ->whereNull('deleted_at'),
            ],
            'is_accepted' => 'required|in:1,2',
            'notification_id' => 'required|exists:rider_notifications,id'
        
        ];
    }
    
    public function messages()
    {
        return [
            'parcel_request_id.unique' => 'Parcel has already been accepted.',
        ];
    }

   
    
    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for AcceptParcelRequest', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' => $firstError,
        //     'data' => [],
        // ], 422));

        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));

    }
}
