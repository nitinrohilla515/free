<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;

class UpdateCoordinateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
            // 'latitude' => 'required|regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', 
            //  'longitude' => 'required|regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'
            'latitude' => ['required', 'regex:^[-+]?([0-8]?[0-9]|90)(\.\d+)?$^'],
            // 'longitude' => ['required', 'regex:^[-+]?([0-1][0-7][0-9]|180)(\.\d+)?$^'],
            'longitude' => ['required', 'regex:^[-+]?([0-8]?[0-9]|90)(\.\d+)?$^'],


            
        
        ];
    }

    public function messages()
{
    return [
        'latitude.required' => 'The latitude field is required.',
        'latitude.between' => 'The latitude must be between -90 and 90.',
        'latitude.regex' => 'The latitude format is invalid.',
        'longitude.required' => 'The longitude field is required.',
        'longitude.between' => 'The longitude must be between -180 and 180.',
        'longitude.regex' => 'The longitude format is invalid.',
    ];
}
  

    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for Update Coordinates', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' =>  $firstError,
            'data' => $validator->errors(),
        ], 422));
    }
}
