<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;

class VerifyOtpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'mobile' => 'required|numeric|digits:10',
            'otp' => 'required|numeric|digits:4',

        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for VerifyOtp', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' =>  $firstError,
            'data' => $validator->errors(),
        ], 422));
    }

}
