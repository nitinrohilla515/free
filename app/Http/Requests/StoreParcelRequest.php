<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;


class StoreParcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'pickup_user_id' => 'required|exists:users,id',
            'receiver_user_id' => 'nullable|exists:users,id|different:pickup_user_id',
            'display_name' => 'required|string',
            'parcel_type' => 'required|in:paper,glass,hard,others', 
            'parcel_worth' => 'required|numeric|between:0, 9999999999.99',
            // 'parcel_worth' => ['required', 'numeric', new MaxDecimalValue(99999999.99)],

            'parcel_details' => 'required|string',
            'item_weight' => 'required',
            'is_create' => 'required|in:0,1', 
            // 'payment_method' => $this->input('type') == 1 ? 'required' : '',
        ];
    }

    public function messages()
    {
        return [
            'receiver_user_id.different' => 'The pickup user and receiver user must be different.',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for StoreParcelRequest', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

    

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' => $firstError,
        //     'data' => $validator->errors(),
        // ], 422));
        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));

    }
}
