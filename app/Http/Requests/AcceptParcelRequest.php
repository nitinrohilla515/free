<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;


class AcceptParcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'parcel_request_id' => [
                'required',
                'exists:parcel_requests,id',
                Rule::unique('all_parcels', 'parcel_request_id')
                    ->whereNull('deleted_at'),
            ],
            'rider_user_id' => 'required|exists:delivery_people,id',
        ];
    }
    
    public function messages()
    {
        return [
            'parcel_request_id.unique' => 'Parcel has already been accepted.',
        ];
    }

   
    
    protected function failedValidation(Validator $validator)
    {
        $firstError = $validator->errors()->first();

        Log::channel('version1')->error('Validation failed for AcceptParcelRequest', [
            'user_id' => $this->input('user_id'),
            'validation_errors' => $validator->errors(),
        ]);

        // throw new HttpResponseException(response()->json([
        //     'success' => false,
        //     'message' =>  $firstError,
        //     'data' => $validator->errors(),
        // ], 422));
        throw new HttpResponseException(response()->json(falseResponse($firstError, $validator->errors()), 422));

    }
}
