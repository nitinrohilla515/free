<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReportReason;
use App\Models\RiderRoprt;
use Illuminate\Support\Facades\Log;


use App\Http\Requests\Rider\ReportRequest;

class ReportController extends Controller
{
    //

    
    public function reportReasonList(Request $r){

        $reasonList = ReportReason::select('id', 'report_reason')->get();
        return response()->json(successResponse('Returned report reason list', [ 'list' => $reasonList ]), 200);

     

    }



    public function saveReport(ReportRequest $r)
    {
        try {
            $riderId = $r->user_id;
            $report = new RiderRoprt;
            $report->parcel_id = $r->parcel_id;
            $report->report_reason_id = $r->report_reason_id;

            $report->report = $r->report;
            $report->rider_id = $riderId;
        
            if ($r->hasFile('image')) {
                $file = $r->file('image');
                $path = 'riderReport/'; // You can adjust the path as needed
                $filename = saveFile($file, $path);
                $report->image = $filename; // Update the file path in the UserReport model
               
            }
            $report->save();
          
            Log::channel('version1')->info('Rider report submitted  successfully', ['report_id' => $report->id]);
            return response()->json(successResponse('Rider report submitted  successfully', [ 'list' => $report ]), 201);

        
        } catch (\Exception $e) {
            return response()->json(falseResponse('Failed to submit report.', [ 'error' => $e->getMessage() ]), 500);

        }
    }
     
    
    public function saveProfile(StoreProfileRequest $r)
    {
        try {
            $riderId = $r->user_id;
            $report = new RiderRoprt;
            $report->report_reason_id = $r->report_reason_id;

            $report->report = $r->report;
            $report->rider_id = $riderId;
        
            if ($r->hasFile('image')) {
                $file = $r->file('image');
                $path = 'riderReport/'; // You can adjust the path as needed
                $filename = saveFile($file, $path);
                $report->image = $filename; // Update the file path in the UserReport model
               
            }
            $report->save();
          
            Log::channel('version1')->info('Rider report submitted  successfully', ['report_id' => $report->id]);
            return response()->json(successResponse('Rider report submitted  successfully', [ 'rider' => $report ]), 201);

        
        } catch (\Exception $e) {
            return response()->json(falseResponse('Failed to submit report.', [ 'error' => $e->getMessage() ]), 500);

         
        }
    }

}
