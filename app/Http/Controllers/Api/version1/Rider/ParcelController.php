<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ParcelRequest;
use App\Models\AcceptParcel;
use App\Models\RiderNotification;
use App\Models\OrderJourney;
use App\Models\UserRating;


use App\Models\User;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Requests\Rider\AcceptParcelRequest;
use App\Http\Requests\Rider\OrderJourneyRequest;
use App\Http\Requests\User\ParcelSummaryRequest;


use Illuminate\Support\Facades\Log;
use App\Services\FirebaseService;


class ParcelController extends Controller
{
    protected $firebaseService;

    public function __construct(FirebaseService $firebaseService)
    { 
        $this->firebaseService = $firebaseService;
    }
  
    //

    public function acceptRejectParcel(AcceptParcelRequest $r){
           
      
        try {
            $riderId = $r->user_id;
            // $riderId = 3;
            $findNotification = RiderNotification::find($r->notification_id);
            if($findNotification->rider_id !== $riderId){
                return response()->json(falseResponse('Invalid rider.', []), 401);
            }

            $checkriderParcel = AcceptParcel::where('rider_user_id', 3)->whereBetween('status', ['1', '5'])->get();
            if(!$checkriderParcel->isEmpty()) {
                return response()->json(falseResponse('You cant accept new parcel until you have any active parcel.', []), 401);
            }

            $pickupCode = mt_rand(100000, 999999); 
            $receiverCode = mt_rand(100000, 999999);
            // $userId = auth()->guard('rider')->user()->id;
            // $riderId = $r->user_id;

            if($findNotification->data['is_accepted'] == 1 || $findNotification->data['is_accepted'] == 2 ){
                return response()->json(falseResponse('Notification expired', []), 401);
            }


            $notificationData = $findNotification->data;
            // $notificationData['status'] = 1;
            $notificationData['is_accepted'] = $r->is_accepted;
            $findNotification->data = $notificationData;
            $findNotification->save();
            $parcelRequest = ParcelRequest::find($r->parcel_request_id);
            $parcelRequest->is_accepted = 1;
            $parcelRequest->save();
            $riderId = $findNotification->data['notificationRiderId'];
            $parcel = [];
            if($r->is_accepted === 1){
            $parcel = AcceptParcel::create([
                'parcel_request_id' => $r->parcel_request_id,
                'pickup_code' => $pickupCode,
                'receiver_code' => $receiverCode,
                'status' => '1',
                'rider_user_id' => $riderId,
                'is_accepted' => true
                
            ]);
        }

          
            Log::channel('version1')->info('Parcel accepted by delivery person successfully', ['parcel_id' => $r->parcel_request_id]);
            return response()->json(successResponse('Parcel accepted successfully', ['parcel' => $parcel]), 201);

          
        } catch (\Exception $e) {
            Log::channel('version1')->info('Failed to place parcel', ['error' => $e->getMessage()]);

            return response()->json(falseResponse('Failed to place parcel', ['error' => $e->getMessage()]), 500);

        }

    }

    public function previousRides(Request $r){
        $today = Carbon::today();
        $riderId = $r->user_id;
        // $riderId = 3;
          try{
           $allParcels = AcceptParcel::where('rider_user_id', $riderId)->with(['parcelRequest' => function ($query) {
            $query->select('id', 'display_name', 'parcel_type', 'parcel_worth', 'parcel_details', 'coordinate_data', 'amount', 'distance', 'item_weight', 'payment_method', 'created_at');
        }])->get();

       $filteredParcels =  $allParcels->transform(function ($parcel) use ($riderId) {
         
        
            $responseParcel = [
                'id' => $parcel->parcelRequest->id,
                'pickup_user_id' => $parcel->pickup_user_id,
                'receiver_user_id' => $parcel->receiver_user_id,
                'coordinate_data' => $parcel->parcelRequest->coordinate_data,
                'display_name' => $parcel->parcelRequest->display_name,
                'parcel_type' => $parcel->parcelRequest->parcel_type,
                'parcel_worth' => $parcel->parcelRequest->parcel_worth,
                'parcel_details' => $parcel->parcelRequest->parcel_details,
                'amount' =>$parcel->parcelRequest->amount,
                'distance' =>$parcel->parcelRequest->distance,
                'payment_method' =>$parcel->parcelRequest->payment_method,
                'submit_by_user' => $parcel->parcelRequest->submit_by_user,
                'item_weight' => $parcel->parcelRequest->item_weight,
                'created_at' => $parcel->parcelRequest->created_at,
                'cancelled' => false,
                'status' => null,
            ];
        
          
            
            $responseParcel['cancelled'] = $parcel->parcelRequest->cancelledParcel ? true : false;
          
            if ($parcel->status != '6' && !$responseParcel['cancelled']) {
                return null;
            }
            if ($responseParcel['cancelled']) {
                $responseParcel['status'] = 'Cancelled';
            } else {
                if ($parcel->status === '5') {
                    $responseParcel['status'] = 'Delivered';
                }
                
            }
        
            return $responseParcel;
        });
        $filteredParcels = $filteredParcels->filter(); 
        $filteredParcels = $filteredParcels->values(); 
        
        return response()->json(successResponse('All previous rides returned successfully', ['allParcels' => $filteredParcels]), 200);
    } catch (\Exception $e) {
        Log::channel('version1')->error('Failed to return previous rides ', ['error' => $e->getMessage()]);

        return response()->json(falseResponse('Failed to return previous rides ', ['error' => $e->getMessage()]), 500);

    }

    }

    public function allParcelRequests(Request $r){
        $today = Carbon::today();
        $riderId = $r->user_id;
        // $riderId = 3;
       try{
        $getRequestNotifications = RiderNotification::where('rider_id', $riderId)->where('type', '2')->orderBy('created_at', 'desc')->get();
        // return $getRequestNotifications;
        $result = [];
        $currentDateTime = Carbon::now();
            foreach ($getRequestNotifications as $notification) {
             // Check if 'is_accepted' is not equal to 2
             $notificationData = $notification->data['parcel_data'];
             $getParcels = ParcelRequest::where('id', $notificationData['id'])->with(['acceptedParcels','cancelledParcel', 'orderJourney', 'pickupUser', 'reciverUser'])->get()->first();
            //   $status = $getParcels->acceptedParcels;
            //   return $status;
            $createdAt = Carbon::parse($notification->created_at);
$timeDifference = $currentDateTime->diffInMinutes($createdAt);

             

            //  if (isset($notification->data['is_accepted']) && $notification->data['is_accepted'] !== 2) {
                    // Check if cancelledParcel is not set and the status of accepted parcels is not '6'
                   if($getParcels){
                    if ($timeDifference >= 45 && ($notification->data['is_accepted'] ?? '') == 0) {
                        continue; // Skip this iteration
                    }
                    if($getParcels->acceptedParcels && $getParcels->acceptedParcels->rider_user_id !== $riderId){
                       continue;
                    }
                    if (!isset($getParcels->cancelledParcel) && (!isset($getParcels->acceptedParcels) || ($getParcels->acceptedParcels->status ?? '') !== '6') && ( $notification->data['is_accepted'] ?? '') != 2) {

                     
     




                $submitByUserId = $notificationData['submit_by_user'];

           
            $userProfile = User::select('id', 'name', 'profile_photo')->withTrashed()->find($submitByUserId);

            $modifiedData = $notification->data;
            $modifiedData['parcel_data']['submit_by_user_profile'] = $userProfile;
            $modifiedData['parcel_data']['pickupUser'] = $getParcels->pickupUser;
            $modifiedData['parcel_data']['receiverUser'] = $getParcels->reciverUser;

            $modifiedData['parcel_data']['order_journey_status'] = $getParcels->orderJourney ? $getParcels->orderJourney->journey_status : null;

      
        
            // Assign the modified data back to the notification
            $notification->data = $modifiedData;


         
            // Add modified notification data to the result array
            $result[] = $notification;
                        }
                    
                    }
        // }
                    

        }


         

$riderSummary = AcceptParcel::getRiderSummary($riderId);

$totalDistanceToday = $riderSummary['totalDistanceToday'];
$totalEarningsToday = $riderSummary['totalEarningsToday'];
$totalRidersToday = $riderSummary['totalRidersToday'];

// return $todayParcels;


return response()->json(successResponse('Parcel requests returned successfully', ['parcelRequests' => $result, 'total_distance_today' => $totalDistanceToday, 'total_earnings_today' => $totalEarningsToday,
'total_riders_today' => $totalRidersToday]), 200);
} catch (\Exception $e) {
    Log::channel('version1')->error('Failed to return parcel requests ', ['error' => $e->getMessage()]);

    return response()->json(falseResponse('Failed to return parcel requests ', ['error' => $e->getMessage()]), 500);

}




    }



public function orderJourney(OrderJourneyRequest $r){
    $riderId = $r->user_id;
    // $riderId = 1;
    $status = $r->status;

  

        try{
            $getParcel = AcceptParcel::where('parcel_request_id', $r->parcel_id);

            $existingStatus = OrderJourney::where('parcel_id', $r->parcel_id)
            ->where('journey_status', '>=', $status)
            ->exists();
            if($existingStatus){
                return response()->json(falseResponse('Status is not valid', []), 401);


            }

    if ($status === '3' || $status === '6') {
        if (!isset($r->code)) {
            return response()->json(falseResponse(($status === 3) ? 'Parcel pickup code is required' : 'Parcel prop code is required', []), 401);
        } else {
            // Assuming 'code' is the column name in the AcceptParcel model where codes are stored
            $column = $status === '3' ? 'pickup_code' : 'receiver_code';
            $codeExists = $getParcel->where($column, $r->code)->exists();
            
            if (!$codeExists) {
                return response()->json(successResponse('Order status updated.', ['is_verified' => false]));
            }
        }
    }
   
        $key = ($status === '1' || $status === '2' || $status === '3') ? 'pickup' : 'drop';
        $subkey = ($status === '1' || $status === '4') ? 'started_at' : (($status === '2' || $status === '5') ? 'reached_at' : 'verified_at');

      

        $orderJourney = OrderJourney::updateOrCreate(
            ['parcel_id' => $r->parcel_id, 'rider_id' => $riderId],
            [
                'journey_status' => $status,
            ]
        );
        
        $data = $orderJourney->data ?? [];
        
        if (isset($data[$key])) {
            // If it exists, append the new subkey to the existing sub-array
            $data[$key][$subkey] = Carbon::now()->toDateTimeString();
        } else {
            // If it doesn't exist, create a new sub-array with the subkey
            $data[$key] = [
                $subkey => Carbon::now()->toDateTimeString(),
            ];
        }

        // Update the 'data' column
        $orderJourney->update(['data' => $data]);

        if ($status === '3' || $status === '4' || $status === '5' || $status === '6') {
            // Update the status in ParcelRequest table
            $getParcel->update(['status' => $status]);
        }
       

        $getJourneyStatusText = journeyStatusText($status);
        $getRceiverUser = $getParcel->with('parcelRequest', 'parcelRequest.submitByUser', 'parcelRequest.pickupUser', 'parcelRequest.reciverUser')->first();
        // status 2 or 4 then send pickup code or receiving code notification
        if($status === '2' || $status === '4'){
            $title = $status === '2' ? 'Pickup code' : 'Receiving code';
            $bodyText = $status === '2' ? 'pickup' : 'receiving';
            $body = 'Share this code with delivery person to confirm your '. $bodyText.'';
            $token = $status === '2' ? $getRceiverUser->parcelRequest->pickupUser->fcm_token : $getRceiverUser->parcelRequest->reciverUser->fcm_token;
            $toUser = $status === '2' ? $getRceiverUser->parcelRequest->pickupUser->id : $getRceiverUser->parcelRequest->reciverUser->id;
            $code = $status === '2' ? $getRceiverUser->pickup_code : $getRceiverUser->receiver_code;
            $data = ['code' => $code, 'parcel_id' => $r->parcel_id];
            $notificationType = '2';

           createNotification($toUser, $notificationType, $title, $body, $data);
            $this->firebaseService->sendNotification($token, $title, $body, $data);
        }
        $getRceiverUserToken = $getRceiverUser->parcelRequest->submitByUser->fcm_token;

        $title = $getJourneyStatusText['title'];
         $body = $getJourneyStatusText['body'];
        $data = ['parcel_id' => $r->parcel_id]; // Extra data
        $notificationType = '3'; // 3 for parcel journey notification s
        $createNotification = createNotification($getRceiverUser->parcelRequest->submit_by_user, $notificationType, $title, $body, $data);
        $this->firebaseService->sendNotification($getRceiverUserToken, $title, $body, $data);






        // return response()->json(successResponse('Order status updated.', []));
        return response()->json(successResponse('Order status updated.', $status === '3' || $status === '6' ? ['is_verified' => true] : []));



    } catch (\Exception $e) {
        Log::channel('version1')->error('Failed to return process requests of order journey', ['error' => $e->getMessage()]);
    
        return response()->json(falseResponse('Failed to process requests ', ['error' => $e->getMessage()]), 500);
    
    }
    

 
   
}

    public function parcelDetail(Request $r, $id){
        try{
            $getParcel = ParcelRequest::with([
                'pickupUser',
                'reciverUser',
                'acceptedParcels' => function ($query) {
                    $query->select('id', 'rider_user_id', 'parcel_request_id', 'status');
                },
                'acceptedParcels.rider' => function ($query) {
                    $query->select('id', 'name', 'latitude', 'longitude');
                    // Include the profile relation from the Rider model
                    $query->with(['profile' => function ($query) {
                        // Select only the profile_photo field
                        $query->select('rider_id','profile_photo');
                    }]);
                },'orderJourney'
            ])->find($id);
            
            
            // Calculate and assign rider ratings
            if($getParcel){
           
                $userRating = calUserRating($getParcel->submit_by_user);
                
                $getParcel->rating = $userRating;
            
        }
        else{
            $getParcel->rating = null;


        }
            return response()->json(successResponse('Pracel detail fetched.', ['data' => $getParcel]));

        } catch (\Exception $e) {
            Log::channel('version1')->error('Failed to return parcel detail', ['error' => $e->getMessage()]);
        
            return response()->json(falseResponse('Internal server error ', ['error' => $e->getMessage()]), 500);
        
        }
    }


}




