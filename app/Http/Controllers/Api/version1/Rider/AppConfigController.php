<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AppConfig;

class AppConfigController extends Controller
{
    //

    public function appConfig(){
        $getConfigdata = AppConfig::select('key', 'value')->get();
        return response()->json(successResponse('App config data returned successfully.', ['data' => $getConfigdata]), 201);
    }
}
