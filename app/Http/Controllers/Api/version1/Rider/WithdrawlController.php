<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Rider\AddBankRequest;
use App\Http\Requests\Rider\InitiateTransfer;

use Illuminate\Support\Facades\Http;
use Paystack;
use Illuminate\Support\Facades\Log;
use App\Models\RiderBankAccount;
use App\Models\Rider;

use Illuminate\Validation\Rule;






class WithdrawlController extends Controller
{
    //

    public function addBank(AddBankRequest $r){
    
        try{
        
          
            $riderId = $r->user_id;
            // $riderId =1 ;
            // $riderId = 1;
        $url = "https://api.paystack.co/transferrecipient";
        $secretKey = env('PAYSTACK_SECRET_KEY');

        // $resposne = Paystack::addBankAccount();
        // return $resposne;

        $fields = [
            "type" => $r->type,
            "name" => $r->name,
            "account_number" => $r->account_number,
            "bank_code" => $r->bank_code,
            "currency" => $r->currency
          ];
       

        // Add secret key to headers
        $headers = [
            'Authorization' => 'Bearer ' . $secretKey,
            'Cache-Control' => 'no-cache',
        ];

    


        $response = Http::withHeaders($headers)->post($url, $fields);
        $responseJson =  $response->json();

        if($response->successful()){
            $saveBank = new RiderBankAccount;
            $saveBank->rider_id = $riderId;
            $saveBank->bank_name = $responseJson['data']['details']['bank_name'];
            $saveBank->account_number = $responseJson['data']['details']['account_number'];
            $saveBank->recipient_code = $responseJson['data']['recipient_code'];
            $saveBank->save();


            
            return response()->json(successResponse($responseJson['message'], $response), 200);

        }

        return response()->json(falseResponse($responseJson['message'], $responseJson), 500);

        // Return response
    }
        catch (\Throwable $e) { 
         
                $response = [
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'function' => $e->getTrace()[0]['function'] ?? 'N/A',
                ];
                $message = 'Failed to add bank: Unknown error';
            
            Log::channel('version1')->error($message, $response );



        return response()->json(falseResponse($message, $response), 500);

    
    }
    }

    public function bankList(Request $r){
        $riderId = $r->user_id;
        // $riderId = 1;
        $getBanks = RiderBankAccount::select('rider_id', 'bank_name', 'account_number', 'recipient_code')->where('rider_id', $riderId)->get();
        return response()->json(successResponse("Banks retrived", ['list' => $getBanks]), 200);
    }


    // public function initiateTransfer(InitiateTransfer $r){
    //     $url = "https://api.paystack.co/transfer";
    //     $secretKey = env('PAYSTACK_SECRET_KEY');
    //     $riderId = $r->user_id;
    //     $riderId = 3;

    //     $getBank = RiderBankAccount::find($r->bank_id);
    //     $rider = Rider::findOrFail($riderId);
    //     $totalEarnings = $rider->totalEarnings;
    //     if($r->amount > $totalEarnings){
    //         return response()->json(falseResponse("Can't withdraw more than your earnings", []), 500);

    //     }

    //     // $resposne = Paystack::addBankAccount();
    //     // return $resposne;

    //     $fields = [
    //         "source" => "balance", 
    //         "reason" => "Calm down", 
    //         "amount" => $r->amount, 
    //         "recipient" => $getBank->recipient_code
    //         ];
       

    //     // Add secret key to headers
    //     $headers = [
    //         'Authorization' => 'Bearer ' . $secretKey,
    //         'Cache-Control' => 'no-cache',
    //     ];

    


    //    return $response = Http::withHeaders($headers)->post($url, $fields);
    // }



}

