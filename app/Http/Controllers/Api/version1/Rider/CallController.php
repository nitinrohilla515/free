<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Rider;
use App\Models\ParcelRequest;
use App\Models\CallHistory;





use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Requests\Rider\Call;
use App\Http\Requests\Rider\CallRequest;


use Illuminate\Support\Facades\Log;
use App\Services\FirebaseService;


class CallController extends Controller
{
    //
    protected $firebaseService;
    

    public function __construct(FirebaseService $firebaseService)
    { 
        $this->firebaseService = $firebaseService;
    }


    public function callRequest(CallRequest $r){

        try{
            $riderId = $r->user_id;
            // $riderId = 1;

          // Create a new message
          if($r->type === 'start'){
    $message = new CallHistory();
    $message->user_id = $r->user;
    $message->rider_id = $riderId;
    $message->parcel_id = $r->parcel_id;

    $message->call_type = 'rider_initiated';
    $message->save();
          }

    $findUser = User::select('id','name', 'fcm_token')->findOrfail($r->user);
    $findRider = Rider::select('id','name', 'fcm_token')->with('profile')->findOrfail($riderId);
    $findParcel= ParcelRequest::with('acceptedParcels')->findOrfail($r->parcel_id);

    


     $status = false;
     $message = 'Call request processed';
    if($r->type === 'end'){
        $status = true;
        $message = 'Call end request processed';
    }

    $title = 'Call request.';
     $body = "call notification";
     $data = [
        'parcel_id' => $r->parcel_id,
        'call_status' => $status,
        'sender_id' => $findRider->id,

        'sender_name' => $findRider->name,
        'sender_name' => $findRider->name,
        'sender_photo' => $findRider->profile->profile_photo ?? null,

        'receiver_id' => $findUser->id,

        'receiver_name' => $findUser->name,
        'receiver_photo' => $findUser->profile_photo,
        'status' => $findParcel->acceptedParcels->status ?? null

        // Add any other data as needed
    ];
    // $data = ['sender' => $findRider, 'receiver' => $findUser, 'parcel_id' => $r->parcel_id, 'call_status' => $status]; // Extra data


    $token = $findUser->fcm_token;

    $this->firebaseService->sendNotification($token, $title, $body, $data);
    return response()->json(successResponse($message, []), 200);


}  catch (\Throwable $e) {
    $response = [
        'message' => $e->getMessage(),
        'file' => $e->getFile(),
        'line' => $e->getLine(),
        'function' => $e->getTrace()[0]['function'] ?? 'N/A',
    ];
    $message = 'Internal server error';
     // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
    // Log::channel('version1')->error('Failed to create parcel: Unknown error', );

Log::channel('version1')->error($message, $response );



return response()->json(falseResponse($message, $response), 500);
}


    }
}
