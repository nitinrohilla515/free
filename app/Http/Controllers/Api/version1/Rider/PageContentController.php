<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RiderPageContent;


class PageContentController extends Controller
{
    //
    public function PrivacyPolicy(){
        $getContent = RiderPageContent::first();

        return response()->json(['success' => false, 'message' => 'Privacy policy page content returned ', 'data' => [ 'pageContent' => $getContent->privacy_policy ]], 200);

    }

    public function TermsConditions(){
        $getContent = RiderPageContent::first();

        return response()->json(['success' => false, 'message' => 'Privacy policy page content returned ', 'data' => [ 'pageContent' => $getContent->terms_conditions ]], 200);

    }
}
