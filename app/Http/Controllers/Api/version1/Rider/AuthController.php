<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\Rider\SignupSigninRequest;
use App\Http\Requests\Rider\GenerateTokenRequest;
use Illuminate\Support\Facades\Log;
use App\Models\Rider;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Carbon\Carbon;
use App\Services\AuthService;



class AuthController extends Controller
{
    //
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function signUpsignIn(SignupSigninRequest $r)
    {
        try {
            // $uid = $r->header('uid');
            // if (!$uid) {
            //     return response()->json(falseResponse('UID not found in headers', []), 401);

            //     // return response()->json(['success' => false, 'message' => 'UID not found in headers', 'data' => []], 401);

            // }
    
            // $isUserVerified = $this->authService->verifyUser($uid);
    
            // if (!$isUserVerified) {
            //     return response()->json(falseResponse('Rider not verified', []), 401);

            //     // return response()->json(['success' => false, 'message' => 'Rider not verified', 'data' => []], 401);
            // }
    
            $mobile = $r->input('mobile');
            $existingRider = Rider::where('mobile', $mobile)->whereNotNull('mobile_verified_at')->first();
            if ($existingRider) {
                $existingRider->fcm_token = $r->fcm_token;
                $existingRider->device_id = $r->device_id;
                $existingRider->save();
             
                return response()->json(successResponse('Rider already exists', ['rider' => $existingRider]), 200);

    
            } else {
                $countryId = findCountryId($r->country_code);

                $rider = Rider::updateOrCreate(
                    [
                        'mobile' => $r->mobile,
                        'country_id' => $countryId->id,
                    ],
                    [
                        'password' => Hash::make(Str::random(8)),
                        'fcm_token' => $r->fcm_token,
                        'device_id' => $r->device_id
                    ]
                );

                Log::channel('version1')->info("New rider created with mobile: $mobile");
                return response()->json(successResponse('Rider account created', ['rider' => $rider]), 200);
             
            }
        } catch (\Exception $e) {
            Log::channel('version1')->error("Error in signUpsignIn: " . $e->getMessage());
            return response()->json(falseResponse('Internal Server Error', ['error' => $e->getMessage()]), 500);

        
        }
    }
    


public function generateToken(GenerateTokenRequest $r) {
   
    try {
        $uid = $r->uid;

        if(!$uid){
            return response()->json(falseResponse('UID not found in headers', []), 401);

    
        }
        if (!$this->authService->verifyUser($uid)) {
            return response()->json(falseResponse('User not verified', []), 401);

        }
        $token = $this->authService->generateToken($uid, $r->riderid, $r->device_id);
        return $token;

        $rider = Rider::find($r->riderid);
        $rider->mobile_verified_at = Carbon::now();
        $rider->fcm_token = $r->fcm_token ? $r->fcm_token : null;
        $rider->device_id = $r->device_id ? $r->device_id : null;

        $rider->save();
        
            Log::channel('version1')->info("Token generated for rider: $rider. JWT token created.");
            return response()->json(successResponse('Token generated for rider', ['rider' => $rider,
            'token' => $token,
            'token_type' => 'bearer',]), 201);

         
            
       
    } catch (\Exception $e) {
        Log::channel('version1')->error("Error in generate Token: " . $e->getMessage());
        return response()->json(falseResponse('Internal Server Error', []), 500);

    }
    }



    /**
     * Refresh jwt token for user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken(){
        $token = JWTAuth::parseToken()->refresh();
      
        return response()->json([
            'success' => true,
            'message' => 'Token refreshed',
            'data' => [
                'tokenHash' => $token,
                'token_type' => 'bearer',
            ],
        ], 200);

    }
    
 /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->json(successResponse('User successfully signed out', []), 200);

    }
}
