<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rider;
use App\Models\Riderprofile;
use App\Models\UserRating;
use App\Models\ParcelRequest;
use App\Models\RiderNotification;

use Carbon\Carbon;

use JWTAuth;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\Rider\StoreProfileRequest;
use App\Http\Requests\UpdateCoordinateRequest;
use App\Http\Requests\Rider\SubmitRating;
use App\Http\Requests\Common\UpdateToken;




class RiderController extends Controller
{
   
    //

       /**
     * Get the authenticated Rider.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $r) {

        $riderId = $r->user_id;
        $getRider = Rider::with('profile', 'countryInfo')->findOrFail($riderId);
        return response()->json(successResponse('Rider profile returned', [ 'rider' => $getRider ]), 200);


    }

       /**
     * Get the authenticated Rider.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleStatus(Request $r) {
        // $rider = auth()->guard('rider')->user();
        $riderId = $r->user_id;
        // $riderId = 1;


        $getRider = Rider::findOrFail($riderId);
        
    $newStatus = $getRider->status === 0 ? 1 : 0;
    $getRider->status = $newStatus;
    $getRider->save();
    return response()->json(successResponse('Status changed', [ 'rider' => $getRider ]), 200);


    }

      /**
     * Update the rider's profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(StoreProfileRequest $r)
    {
        try {
            $riderId = $r->user_id;
            // $riderId = 1; 
            $getRider = Rider::with('profile')->findOrFail($riderId);
            $getRider->email = $r->email;
            $getRider->name = $r->name;
            $getRider->save();
            Log::channel('version1')->info("email for update profile: $r->email. JWT token created.");


            
            
            // Update or create RiderProfile data
            if($r->has('profile_photo')){  
                $profilePhoto = saveFile($r->file('profile_photo'), 'RiderProfiles');
            }
            else{
                $profilePhoto = $getRider->profile->profile_photo;
            }
            if($r->has('driving_license_file')){  
                $dlfile = saveFile($r->file('driving_license_file'), 'DLFiles');
            }
            else{
                $dlfile = $getRider->profile->driving_license_file;
            }
            if($r->has('vehicle_registration_file')){  
                $vrfile = saveFile($r->file('vehicle_registration_file'), 'vehicleRegistrationFiles');
            }
            else{
                $vrfile = $getRider->profile->vehicle_registration_file;
            }
            $riderProfile = $getRider->profile()->updateOrCreate(
                ['rider_id' => $riderId],
                [
                    'address' => $r->address,
                    'profile_photo' => $profilePhoto,
                    'age' => $r->age,
                    'driving_license_no' => $r->driving_license_no,
                    'driving_license_file' => $dlfile,
                    'vehicle_registration_no' => $r->vehicle_registration_no,
                    'vehicle_registration_file' => $vrfile,
                ]
            );
            
            // Reload the Rider model with the updated profile
            $getRider->load('profile');
            return response()->json(successResponse('Rider profile updated successfully', [ 'riderProfile' => $getRider]), 200);

        } catch (\Exception $e) {
            Log::channel('version1')->error('Failed to update profile.', ['error' => $e->getMessage()]);


            return response()->json(falseResponse('Failed to update profile.', [ 'error' => $e->getMessage() ]), 500);
       
        }
    }

    public function updateCoordinate(UpdateCoordinateRequest $r)
    {
        try {
            $riderId = $r->user_id;
            $getRider = Rider::findOrFail($riderId);
            $getRider->latitude = $r->latitude;
            $getRider->longitude = $r->longitude;
            $getRider->save();
    
          
    
            return response()->json(successResponse('User Coordinates updated successfully', ['user' => $getRider]), 200);

       
        } catch (\Exception $e) {
            return response()->json(falseResponse('Failed to update coordinates.', [ 'error' => $e->getMessage() ]), 500);

        }
    }


    public function submitRating(SubmitRating $r)
    {
        try {
            $riderId = $r->user_id;
            // $riderId= 1;

            $checkParcel = ParcelRequest::find($r->parcel_id);

            
            $riderRating = UserRating::create([
                    'parcel_id' => $r->parcel_id,
                    'user_id' => $checkParcel->submit_by_user,
                    'rider_id' => $riderId,
                    'rating' => $r->rating
            ]);
    
          
    
            return response()->json(successResponse('Rating submitted', ['riderRating' => $riderRating]), 201);

        
        } catch (\Exception $e) {
            Log::channel('version1')->error('Failed to submit rider rating.', ['error' => $e->getMessage()]);

            return response()->json(falseResponse('Failed to submit rating.', ['error' => $e->getMessage()]), 500);

        }
    }


    public function allNotifications(Request $r)
    {
        $riderId = $r->user_id;
        // $riderId = 1;
        $date = $r->date;
        try{
        $query = RiderNotification::where('rider_id', $riderId);
    
      

        $groupedNotifications = $query->orderBy('created_at', 'desc')->paginate(10)->groupBy(function ($date) {
            $parsedDate = Carbon::parse($date->created_at);
            if ($parsedDate->isToday()) {
                return 'Today';
            } elseif ($parsedDate->isYesterday()) {
                return 'Yesterday';
            } else {
                return $parsedDate->format('F d Y');
            }
        });

        $responseData = [];
foreach ($groupedNotifications as $date => $notifications) {
    $responseData[] = [
        'date' => $date,
        'notifications' => $notifications->map(function ($notification) {
            return [
                'id' => $notification->id,
                'rider_id' => $notification->rider_id,
                'title' => $notification->title,
                'body' => $notification->body,
                'type' => $notification->type,
                'data' => $notification->data,
                'created_at' => $notification->created_at,
                'updated_at' => $notification->updated_at,
                'deleted_at' => $notification->deleted_at,
            ];
        })->toArray(),
    ];
}
        
    
        // }
    
        return response()->json(successResponse('All notifications fetched.', $responseData), 200);
    }  catch (\Throwable $e) {
        $response = [
            'message' => $e->getMessage(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'function' => $e->getTrace()[0]['function'] ?? 'N/A',
        ];
        $message = 'Internal server error';
         // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
        // Log::channel('version1')->error('Failed to create parcel: Unknown error', );
    
    Log::channel('version1')->error($message, $response );
    
    
    
    return response()->json(falseResponse($message, $response), 500);
    }
    }

    public function updateFcmToken(UpdateToken $r){
        try{
            $riderId = $r->user_id;
            // $riderId = 1;
            $rider = Rider::find($riderId);
            $rider->fcm_token = $r->fcm_token;
            $rider->save();

            return response()->json(successResponse('FCM token updated.', []), 200);



        }  catch (\Throwable $e) {
            $response = [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'function' => $e->getTrace()[0]['function'] ?? 'N/A',
            ];
            $message = 'Internal server error';
             // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
            // Log::channel('version1')->error('Failed to create parcel: Unknown error', );
        
        Log::channel('version1')->error($message, $response );
        
        
        
        return response()->json(falseResponse($message, $response), 500);
        }




    }

    public function deleteAccount(Request $r){
        try{
            $riderId = $r->user_id;
            // $riderId = 1;
           $rider = Rider::find($riderId);
           $rider->delete();
           return response()->json(successResponse('Account deleted.', []), 200);


        }  catch (\Throwable $e) {
            $response = [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'function' => $e->getTrace()[0]['function'] ?? 'N/A',
            ];
            $message = 'Internal server error';
             // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
            // Log::channel('version1')->error('Failed to create parcel: Unknown error', );
        
        Log::channel('version1')->error($message, $response );
        
        
        
        return response()->json(falseResponse($message, $response), 500);
        }
    }


}


