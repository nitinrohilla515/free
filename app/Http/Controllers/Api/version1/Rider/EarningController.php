<?php

namespace App\Http\Controllers\Api\version1\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class EarningController extends Controller
{
    //


    public function earnings(Request $r){
        // $riderId = $r->userId;

        // $riderId = 3;
        $riderId = $r->user_id;
        try{
        $rider = Rider::findOrFail($riderId);
        $totalEarnings = number_format(($rider->totalEarnings),2);
        
        $earnings = $rider->acceptedParcels()
            ->where('status', '6') // Filter accepted parcels with status 6 (delivered)
            ->with(['parcelRequest' => function ($query) {
                $query->select('id', 'pickup_user_id', 'receiver_user_id', 'display_name', 'parcel_type', 'parcel_worth', 'parcel_details', 'submit_by_user', 'created_at');
                $query->with(['payment' => function ($query) {
                    $query->select('id', 'parcel_id', 'amount', 'status', 'paid_at');
                }]);
            }])
            ->get(['id', 'parcel_request_id', 'rider_user_id', 'status']);

            Log::channel('version1')->info('Rider earnings returned', ['earnings' => $earnings]);
            return response()->json(successResponse('Rider earnings returned', ['earnings' => $earnings, 'totalEarnings' => $totalEarnings]), 200);
    }  catch (\Throwable $e) {
        $response = [
            'message' => $e->getMessage(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'function' => $e->getTrace()[0]['function'] ?? 'N/A',
        ];
        $message = 'Error in earnings';
         // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
        // Log::channel('version1')->error('Failed to create parcel: Unknown error', );
    
    Log::channel('version1')->error($message, $response );
    
    
    
    return response()->json(falseResponse($message, $response), 500);
    }
    }

    public function rideHistory(Request $r){
        $riderId = $r->user_id;
//  $riderId = 3;
 try{
 $year = $r->input('year', Carbon::now()->year);
 $month = $r->input('month', Carbon::now()->month);
 $rider = Rider::findOrFail($riderId);
 $totalRides = $rider->totalRides;
 $totalEarnings = number_format(($rider->totalEarnings),2);
// $totalEarnings = 0;
 $totalDistance = $rider->totalDistance;
 $monthlyDataCurrentMonth = $rider->getMonthlyData($year, $month);

return response()->json(successResponse('Rider earnings returned', ['totalRides' => $totalRides, 'totalEarnings' => $totalEarnings, 'totalDistance' => $totalDistance, 'monthlyDataCurrentMonth' => $monthlyDataCurrentMonth]), 200);

}  catch (\Throwable $e) {
    $response = [
        'message' => $e->getMessage(),
        'file' => $e->getFile(),
        'line' => $e->getLine(),
        'function' => $e->getTrace()[0]['function'] ?? 'N/A',
    ];
    $message = 'Internal server error';
     // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
    // Log::channel('version1')->error('Failed to create parcel: Unknown error', );

Log::channel('version1')->error($message, $response );



return response()->json(falseResponse($message, $response), 500);
}

 
        
    }
}
