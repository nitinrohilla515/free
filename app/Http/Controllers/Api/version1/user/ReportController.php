<?php

namespace App\Http\Controllers\Api\version1\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReportReason;
use App\Models\UserRoprt;
use Illuminate\Support\Facades\Log;


use App\Http\Requests\User\ReportRequest;



class ReportController extends Controller
{
    //

    public function reportReasonList(Request $r){

        $reasonList = ReportReason::select('id', 'report_reason')->get();
        return response()->json(successResponse('Returned report reason list.', [ 'list' => $reasonList ]), 200);

        // return response()->json(['success' => false, 'message' => 'Returned report reason list', 'data' => [ 'list' => $reasonList ]], 200);

    }



    public function saveReport(ReportRequest $r)
    {
        try {
            $userId = $r->user_id;
            $report = new UserRoprt;
            $report->parcel_id = $r->parcel_id;
            $report->report_reason_id = $r->report_reason_id;

            $report->report = $r->report;
            $report->user_id = $userId;
        
            if ($r->hasFile('image')) {
                $file = $r->file('image');
                $path = 'userReport/'; 
                $filename = saveFile($file, $path);
                $report->image = $filename; 
               
            }
            $report->save();
          
            Log::channel('version1')->info('User report submitted  successfully', ['report_id' => $report->id]);
            return response()->json(successResponse('User report submitted  successfully.', [ 'report' => $report ]), 201);

            // return response()->json(['success' => true, 'message' => 'User report submitted  successfully', 'data' => ['report' => $report]], 201);
        } catch (\Exception $e) {
            return response()->json(falseResponse('Failed to submit report.', [ 'error' => $e->getMessage() ]), 500);

            // return response()->json(['success' => false, 'message' => 'Failed to submit report.', 'data' => ['error' => $e->getMessage()]], 500);
        }
    }

}
