<?php

namespace App\Http\Controllers\Api\version1\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ParcelRequest;
use App\Models\AcceptParcel;

use App\Models\ParcelLog;
use App\Models\AllParcel;
use App\Models\User;
use App\Models\Rider;

use App\Models\UserNotification;

use App\Models\CancelledParcel;

use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Requests\StoreParcelRequest;
use App\Http\Requests\AcceptParcelRequest;
use App\Http\Requests\CancelParcelRequest;
use App\Http\Requests\User\PDNotificationRequest;
use App\Http\Requests\User\PickupDropRequest;
use App\Http\Requests\User\ParcelSummaryRequest;
use App\Http\Requests\User\ParcelPaymentRequest;

use App\Jobs\SendRiderDeliveryRequest;




use Illuminate\Support\Facades\Log;
use App\Services\FirebaseService;
use App\Services\PaystackService;

use Paystack;
use Illuminate\Http\RedirectResponse;




class ParcelController extends Controller
{
    protected $firebaseService,$paystackService;
    

    public function __construct(FirebaseService $firebaseService, PaystackService $paystackService)
    { 
        $this->firebaseService = $firebaseService;
        $this->paystackService = $paystackService;
    }

    public function store(StoreParcelRequest $r)
    {
        try {
            $getPickupUser = User::find($r->pickup_user_id);
            $getReceiverUser = User::find($r->receiver_user_id);

$pickupLocation = $getPickupUser->latitude . ',' . $getPickupUser->longitude;
$receiverLocation = $getReceiverUser->latitude . ',' . $getReceiverUser->longitude;
$lat1 = $getPickupUser->latitude;
$lang1 = $getPickupUser->longitude;
$lat2 = $getReceiverUser->latitude;
$lang2 = $getReceiverUser->longitude;
$unit = 'km';

// if (!is_numeric($lat1) || !is_numeric($lang1) ||
//     !is_numeric($lat2) || !is_numeric($lat2)) {
//     return response()->json(falseResponse('Invalid coordinates. Latitude or longitude is not numeric.', []), 400);
// }
// $lat1 = (float) $getPickupUser->latitude;
// $lang1 = (float) $getPickupUser->longitude;
// $lat2 = (float) $getReceiverUser->latitude;
// $lang2 = (float) $getReceiverUser->longitude;
// $unit = 'km';

// // Check if the latitude and longitude values are valid floats
// if (!is_float($lat1) || !is_float($lang1) || !is_float($lat2) || !is_float($lang2)) {
//     return response()->json(falseResponse('Invalid coordinates. Latitude or longitude is not valid.', []), 400);
// }

if ($lat1 == null || $lang1 == null || $lat2 == null || $lang2 == null) {
    return response()->json(falseResponse('Invalid coordinates. Latitude or longitude cannot be empty.', []), 400);
}

     $checkRiders = checkRiders($lat1, $lang1);
     if(!isset($checkRiders)){
        
        // return response()->json(['success' => false, 'message' => 'Sorry! Rider not available.', 'data' => []], 401);
        return response()->json(falseResponse('Sorry! Rider not available.', ['riders' => $checkRiders]), 401);


     }


$distance = number_format(calculateDistance($lat1, $lang1, $lat2, $lang2, $unit),2);
if (is_string($distance) && strpos($distance, ',') !== false) {
    // Remove the comma from the distance value and convert it to a float
    $distance = floatval(str_replace(',', '', $distance));
} else {
    // If $distance is not a string or doesn't contain a comma, convert it to a float
    $distance = floatval($distance);
}
// $distance = calculateDistance($pickupLocation, $receiverLocation);
// print_r(env('MAX_DELIVERY_DISTANCE'));
// $distance = (float) $distance;
if($distance > env('MAX_DELIVERY_DISTANCE') ){
    return response()->json(falseResponse('Sorry! we can not process your request .', ['distance' => $distance]), 401);

}

$coordinateData = [
    'pickup' => [
        'lat' => $getPickupUser->latitude,
        'long' => $getPickupUser->longitude,
    ],
    'drop' => [
        'lat' => $getReceiverUser->latitude,
        'long' => $getReceiverUser->longitude,
    ],
];
Log::channel('version1')->info('Parcel distance', ['distance' => $distance, 'is_create' => $r->is_create ]);
        //  $calculateAmount = number_format(calculateAmount($r->type, $distance, $r->item_weight), 2);
         $calculateAmount = number_format(calculateAmount($r->type, $distance, $r->item_weight), 2);
$calculateAmountWithoutComma = str_replace(',', '', $calculateAmount);
$userId = $r->user_id;
//  $userId = 1;
         if($r->is_create === 0){
            return response()->json(successResponse('Parcel summary returned.', ['amount' =>    $calculateAmountWithoutComma, 'distance' => $distance, 'delivery_time' => '1 day']), 200);

         }elseif($r->is_create === 1){
            $parcel = ParcelLog::updateOrCreate(
                ['id' => $r->parcel_id], 
                array_merge($r->all(), [
                    'submit_by_user' => $userId,
                    'coordinate_data' => $coordinateData,
                    'amount' => $calculateAmountWithoutComma,
                    'distance' => $distance,
                  
                ])
            );
            Log::channel('version1')->info('Parcel created successfully', ['parcel_id' => $parcel, 'is_create' => $r->is_create ]);
            return response()->json(successResponse('Parcel created successfully.', ['parcel' => $parcel]), 200);
        }
        else{
            Log::channel('version1')->info('In valid is_create value in request parcel', ['is_create' => $r->is_create ]);
            return response()->json(falseResponse('Request failed.', ['request' => $r, 'message' => 'In valid is_create value in request parcel']), 400);

        }
          

           
          
           
         
        } 
        // catch (\Exception $e) {

            // Log::channel('version1')->info('Failed to create parcel', ['error' => $e->getMessage(), 'trace' => $e->getTrace()]);
            // Log::channel('version1')->error('Failed to create parcel: ' . $e->getMessage(), [
            //     'file' => $e->getFile(),
            //     'line' => $e->getLine(),
            //     'function' => $e->getTrace()[0]['function'] ?? 'N/A',
            // ]);
            catch (\Throwable $e) { // Catch all Throwable types
                if ($e instanceof PDOException) {

                    $response = [
                        'message' => $e->getMessage(),
                        'code' => $e->getCode(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'function' => $e->getTrace()[0]['function'] ?? 'N/A',
                    ];
                    $message = 'Failed to create parcel: Database error';
                } else {
                    // Handle other exception types here
                    $response = [
                        'message' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'function' => $e->getTrace()[0]['function'] ?? 'N/A',
                    ];
                    $message = 'Failed to create parcel: Unknown error';
                    // Log::channel('version1')->error('Failed to create parcel: Unknown error', );
                }
                Log::channel('version1')->error($message, $response );



            return response()->json(falseResponse($message, $response), 500);

        
        }
    }

    public function requestParcelSummary(ParcelSummaryRequest $r){
        //  $getParcel = ParcelRequest::with('pickupUser', 'reciverUser')->find($r->parcel_id);
        //   $getParcelStatus = parcelStatusArray();
        //  $getParcel->status = $getParcelStatus;
        //  $getParcel->current_status = getParcelStatus($r->parcel_id);
        try{
        $getParcel = ParcelRequest::with([
            'pickupUser',
            'reciverUser',
            'acceptedParcels' => function ($query) {
                $query->select('id', 'rider_user_id', 'parcel_request_id', 'status');
            },
            'acceptedParcels.rider' => function ($query) {
                $query->select('id', 'name', 'latitude', 'longitude');
                // Include the profile relation from the Rider model
                $query->with(['profile' => function ($query) {
                    // Select only the profile_photo field
                    $query->select('rider_id','profile_photo');
                }]);
            }
        ])->find($r->parcel_id);
        
        if (!$getParcel) {
            return response()->json(falseResponse('Parcel not found for the given id.', []), 404);
        }
        
        $getParcelStatus = parcelStatusArray();
        $getParcel->status = $getParcelStatus;
        $getParcel->current_status = getParcelStatus($r->parcel_id);
        
        // foreach ($getParcel->acceptedParcels ?? [] as $acceptedParcel) {
            if($getParcel->acceptedParcels){
         $riderRating = calRiderRating($getParcel->acceptedParcels->rider->id);
            // $acceptedParcel->rider->rating = $riderRating;
            $getParcel->acceptedParcels->rider->rating = $riderRating;
            }

        // }
        
        // Return the modified parcel request data
        return response()->json(successResponse('Parcel summary returned', ['parcel' => $getParcel]), 200);

        
    } catch (\Exception $e) {
        Log::channel('version1')->error("Error in parcelRequestsummary : " . $e->getMessage());

        return response()->json(falseResponse('Internal server error.', ['error' => $e->getMessage()]), 500);

        // return response()->json(['success' => false, 'message' => 'Internal server error.', 'data' => ['error' => $e->getMessage()]], 500);
    }
        

    }


    public function parcelRequestPayment(ParcelPaymentRequest $r){
        try{
            $userId = $r->user_id;
            // $userId = 1;
        $getParcel = ParcelLog::find($r->parcel_id);
        if($getParcel->status){
            return response()->json(falseResponse('Parcel already created.', []), 400);

        }
        Log::channel('version1')->info('intitiated payment', ['parcel_id' => $getParcel->id]);

      
        if($r->payment_method === "0"){
            $getParcel->payment_method = $r->payment_method;
        $getParcel->save();
        Log::channel('version1')->info('intitiated cod', ['parcel_id' => $getParcel->id]);

            $parcelRequest = createParcelData($getParcel->id);
                Log::channel('version1')->info('Delivery job dispatched successfully', ['parcel_id' => $getParcel->id]);

            $firebaseService = app(FirebaseService::class);
            // SendRiderDeliveryRequest::dispatch($firebaseService, $parcel->id, 1)->onConnection('database')->onQueue('delivery_notification');
            SendRiderDeliveryRequest::dispatchNow($firebaseService, $parcelRequest->id, 5);



            // SendRiderDeliveryRequest::dispatch($firebaseService, $parcel->id, 1)->onConnection('database')->onQueue('default');
            return response()->json(successResponse('Order created with COD', ['parcel' => $parcelRequest, 'is_web'  => false]), 200);

            
            
        } else{
            try{

                $url =  $this->paystackService->makeRequest($getParcel, $userId);

               
                return response()->json(successResponse('Payment link generated.', ['payment_link' => $url, 'is_web'  => true]), 200);

            }catch(\Exception $e) {
                Log::channel('version1')->error("Error in parcelRequestPayment : " . $e->getMessage());


                return response()->json(falseResponse('Internal server error.', ['error' => $e->getMessage()]), 500);

            }
        }
        // $firebaseService = app(FirebaseService::class);
        // SendRiderDeliveryRequest::dispatch($firebaseService, $r->parcel_id, 1);
        // return response()->json(successResponse('Payment method updated', ['parcel' => $getParcel]), 200);
    } catch (\Exception $e) {
        Log::channel('version1')->error("Error in parcelRequestPayment : " . $e->getMessage());

        return response()->json(falseResponse('Internal server error.', ['error' => $e->getMessage()]), 500);

        // return response()->json(['success' => false, 'message' => 'Internal server error.', 'data' => ['error' => $e->getMessage()]], 500);
    }

        // return response()->json(['success' => true, 'message' => 'Payment method updated', 'data' => ['parcel' => $getParcel]], 201);
    }


    public function paymentCallback(Request $r){

        try {
            // $refrence = "MGRdT3tlWkPsRFKCxeD7Gxd1Y";
            $callback = $this->paystackService->handleCallback();
            if($callback['success']){
                $firebaseService = app(FirebaseService::class);
                // SendRiderDeliveryRequest::dispatch($firebaseService, $parcel->id, 1)->onConnection('database')->onQueue('delivery_notification');
                SendRiderDeliveryRequest::dispatchNow($firebaseService, $callback['parcel_id'], 5);
    

            }
            return $callback['success'] 
    ? new RedirectResponse(route('payment.success', ['parcel_id' => $callback['parcel_id']])) 
    : new RedirectResponse(route('payment.failure'));

        } catch (\Exception $e) {
            Log::channel('version1')->error('Payment callback error: ' . $e->getMessage());
           return new RedirectResponse(route('payment.failure'));
            // return redirect()->back()->with('error', 'An error occurred while processing the payment.');
        }
        

    }

    public function sendRiderDeliveryNotification(ParcelSummaryRequest $r){
        try{
        $firebaseService = app(FirebaseService::class);
        SendRiderDeliveryRequest::dispatch($firebaseService, $r->parcel_id, 1);
        return response()->json(successResponse('Rider notification sent successfully', []), 200);

        // return response()->json(['success' => true, 'message' => 'Rider notification sent successfully', 'data' =>[]], 200);
    } catch (\Exception $e) {
        return response()->json(falseResponse('Internal server error.', ['error' => $e->getMessage()]), 500);

        // return response()->json(['success' => false, 'message' => 'Internal server error.', 'data' => ['error' => $e->getMessage()]], 500);
    }
    }

    

   
    public function allParcel(Request $r)
    {
        $userId = $r->user_id;
        // $userId = 2;
        // print_r($userId);
        if (!isset($userId)) {
            return response()->json(falseResponse('Invalid user ID.', []), 400);

            // return response()->json(['success' => false, 'message' => 'Invalid user ID', 'data' => []], 400);
        }

        try{
        $allParcels = ParcelRequest::where(function ($query) use ($userId) {
            $query->where('pickup_user_id', $userId)
                  ->orWhere('receiver_user_id', $userId)
                  ->orWhere('submit_by_user', $userId);
        })->with(['acceptedParcels' => function ($query) {
            $query->with('rider');
        }])
        ->select('id', 'pickup_user_id', 'receiver_user_id', 'display_name', 'parcel_type', 'parcel_worth', 'parcel_details', 'item_weight', 'submit_by_user')
        ->orderBy('created_at', 'desc')->get();
        $filteredParcels = $allParcels->filter(function ($parcel) use ($userId) {
            $pickupUserId = $parcel->pickup_user_id;
            $receiverUserId = $parcel->receiver_user_id;
            $submitUserId = $parcel->submit_by_user;
        
        
            return ($pickupUserId == $userId || $receiverUserId == $userId || $submitUserId == $userId);
        });
            $allParcels->transform(function ($parcel) use ($userId) {
                $pickupUserId = $parcel->pickup_user_id;
                $receiverUserId = $parcel->receiver_user_id;
            
                $responseParcel = [
                    'id' => $parcel->id,
                    'pickup_user_id' => $parcel->pickup_user_id,
                    'receiver_user_id' => $parcel->receiver_user_id,
                    'display_name' => $parcel->display_name,
                    'parcel_type' => $parcel->parcel_type,
                    'parcel_worth' => $parcel->parcel_worth,
                    'parcel_details' => $parcel->parcel_details,
                    'submit_by_user' => $parcel->submit_by_user,
                    'item_weight' => $parcel->item_weight,
                    'type' => null,
                    'code' => null,
                    'cancelled' => false,
                    'status' => null,
                    'rider_name' => null,
                    'rating' => null,
                ];
            
                if ($pickupUserId == $userId) {
                    $responseParcel['type'] = 'Pickup code';
                    $responseParcel['code'] = $parcel->pickup_code;
                } elseif ($receiverUserId == $userId) {
                    $responseParcel['type'] = 'Receiver code';
                    $responseParcel['code'] = $parcel->receiver_code;
                } else {
                    $responseParcel['type'] = "You are not the sender nor the receiver";
                }
            
                // Check if parcel is cancelled
                $responseParcel['cancelled'] = $parcel->cancelledParcel ? true : false;
            
                if ($responseParcel['cancelled']) {
                    $responseParcel['status'] = 'Cancelled';
                } else {
                    if ($parcel->acceptedParcels) {
                            $riderRating = calRiderRating($parcel->acceptedParcels->rider->id);
                               // $acceptedParcel->rider->rating = $riderRating;
                               $responseParcel['rating'] = $riderRating;
                               
                        $responseParcel['status'] = $parcel->acceptedParcels->status;
                        $responseParcel['rider_name'] = $parcel->acceptedParcels->rider->name ?? null;
                
                        $responseParcel['status'] = getStatusText($responseParcel['status']);
                    } else {
                        $responseParcel['status'] = "Pending";
                        $responseParcel['rider_name'] = null;
                    }
                }
            
                return $responseParcel;
            });
            return response()->json(successResponse('Parcels fetched', ['allparcel' => $allParcels]), 200);

            // return response()->json(['success' => true, 'message' => 'Rider notification sent successfully', 'data' =>[]], 200);
        } catch (\Exception $e) {
            Log::channel('version1')->error("Error in pickdropnotification : " . $e->getMessage());

            return response()->json(falseResponse('Internal server error.', ['error' => $e->getMessage()]), 500);
    
            // return response()->json(['success' => false, 'message' => 'Internal server error.', 'data' => ['error' => $e->getMessage()]], 500);
        }
    }

    public function pickupDropNotification(PDNotificationRequest $r){
        $userId = $r->user_id;
        // $userId = 2;
   
    
        

        try {
            $getUser = User::find($userId);
            $getOtherUser = User::find($r->user);
        $token =  $getOtherUser->fcm_token;
        // if ($getOtherUser->latitude === null || $getOtherUser->longitude === null || $getOtherUser->latitude === 0.00 || $getOtherUser->longitude === 0.00) {
        //     return response()->json(falseResponse('Co-ordinates are not saved.', []), 500);
        // }

        // if(!isset($token)){
        //     return response()->json(['success' => false, 'message' => 'FCM token is not saved.', 'data' => ['user' => $getOtherUser]], 422);

        // }
        $title = $r->type == 'drop' ? 'New parcel drop request' : 'New parcel pickup request';
        $bodyPrefix = $r->type == 'drop' ? 'Parcel drop request from ' : 'Parcel pickup request from ';
         $body = $bodyPrefix . $getUser->name;
        $data = ['senderUserId' => $r->user_id, 'notificationUserId' => $getOtherUser->id, 'type' => $r->type, 'status' => 0, 'is_accepted' => 0]; // Extra data
        $notificationType = '1'; // 1 for pick drop request notification 
        $createNotification = createNotification($getOtherUser->id, $notificationType, $title, $body, $data);
        $data['notification_id'] = $createNotification->id;
        $this->firebaseService->sendNotification($token, $title, $body, $data);
        return response()->json(successResponse('Notification sent.', []), 200);

        // return response()->json(['success' => true, 'message' => 'Notification sent.', 'data' => []], 200);
    } catch (Exception $e) {
            // Log or handle the exception as needed
            Log::channel('version1')->error("Error in allParcel : " . $e->getMessage());

            return response()->json(falseResponse('An error occurred while processing the request.', []), 500);

            // return response()->json([
            //     'success' => false,
            //     'message' => 'An error occurred while processing the request.',
            //     'data' => [],
            // ], 500);
        }



    }

    public function pickupDropRequest(PickupDropRequest $r){
       

        try{
            $getUser = User::find($r->user_id);
            $getOtherUser = User::find($r->user);
            $token =  $getUser->fcm_token;
            // if(!isset($token)){
            //     return response()->json(['success' => false, 'message' => 'FCM token is not saved.', 'data' => ['user' => $getOtherUser]], 500);
    
            // }
            $findUserNotification = UserNotification::find($r->notification_id);

            if(isset($findUserNotification->data['is_submitted'])){
                return response()->json(falseResponse('Notification expired', []), 400);

                // return response()->json(['success' => false, 'message' => 'Notification expired', 'data' => []], 500);
            }

            $getUserNotified = User::find($findUserNotification->data['senderUserId']);

            $notificationData = $findUserNotification->data;
            $notificationData['status'] = 1;
            $notificationData['is_accepted'] = $r->status;
            $findUserNotification->data = $notificationData;
            $findUserNotification->save();
      
        $titlePrefix = $r->type == 'drop' ? 'Parcel drop request' : 'Parcel pickup request';
        $status = $r->status == 1 ? 'accepted' : 'rejected';
        $title = $titlePrefix . $status;
         $body = $status;
        $data = ['senderUserId' => $r->user_id, 'notificationUserId' => $getOtherUser->id]; // Extra data

        $this->firebaseService->sendNotification($getUserNotified->fcm_token, $title, $body, $data);
        return response()->json(successResponse('Request ' . $status, []), 200);

        // return response()->json(['success' => true,  'message' => 'Request ' . $status, 'data' => []], 200);
     } catch (Exception $e) {
            // Log or handle the exception as needed
            return response()->json(falseResponse('An error occurred while processing the request.', []), 500);

            // return response()->json([
            //     'success' => false,
            //     'message' => 'An error occurred while processing the request.',
            //     'data' => [],
            // ], 500);
        }


    }


    public function cancelParcel(CancelParcelRequest $r)
    {
        try {
            $userId = $r->user_id;
            $data = $r->all();

            unset($data['user_id']);
            unset($data['appType']);

            $getPayment = ParcelRequest::with('payment')->find($r->parcel_request_id);
            $refundResponse = null;
            if($getPayment->payment_method === '1' && !$getPayment->payment){
                Log::channel('version1')->info('Parcel can not  cancel', ['getPayment' => $getPayment]);

                return response()->json(successResponse('Parcel payment not found.', []), 400);
            }
            if ($getPayment->payment_method === '1') {
                $refundResponse = $this->paystackService->initiateRefund($getPayment, $userId);
                Log::channel('version1')->info('Refund response', ['refundResponse' => $refundResponse]);
            
                if ($refundResponse['status']) {
                    $message = $refundResponse['message'];
                } else {
                    $message = 'Failed to initiate refund.';
                }
            } else {
                $message = 'Parcel cancelled for COD.';
            }
            
            // Create CancelledParcel entry for both online and COD (cash on delivery) payments
            CancelledParcel::create($data);
            
            Log::channel('version1')->info('Parcel cancelled', ['parcel_id' => $r->parcel_request_id]);
            return response()->json(successResponse($message, []), $refundResponse && $refundResponse['status'] ? 200 : 400);
       

            // return response()->json(['success' => true, 'message' => 'Parcel cancelled successfully', 'data' => ['parcel' => $parcel]], 201);
        }  catch (\Throwable $e) {
                $response = [
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'function' => $e->getTrace()[0]['function'] ?? 'N/A',
                ];
                $message = 'Error in cancel parcel';
                 // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
                // Log::channel('version1')->error('Failed to create parcel: Unknown error', );
            
            Log::channel('version1')->error($message, $response );
            
            
            
            return response()->json(falseResponse($message, $response), 500);
            }

    }


    public function activeParcel(Request $r){
         $userId = $r->user_id;
        //  $userId = 2;
        //  $userId = 1;
        // $allParcels = ParcelRequest::with([
        //     'acceptedParcels' => function ($query) {
        //         $query->whereNot('status', '5');
        //     },
        //     'acceptedParcels.rider'
        // ])
        try{
            $allParcels = ParcelRequest::with(['acceptedParcels', 'acceptedParcels.rider'])
            ->whereHas('acceptedParcels') 
            ->orderBy('created_at', 'desc')
            ->get();
    // return $allParcels;
    $filteredParcels = $allParcels->filter(function ($parcel) use ($userId) {
        $pickupUserId = $parcel->pickup_user_id;
        $receiverUserId = $parcel->receiver_user_id;
        $submitUserId = $parcel->submit_by_user;
    
        return ($pickupUserId == $userId || $receiverUserId == $userId || $submitUserId == $userId);
    });
    
    $filteredParcels = $filteredParcels->transform(function ($parcel) use ($userId) {
        $pickupUserId = $parcel->pickup_user_id;
        $receiverUserId = $parcel->receiver_user_id;
    
            $responseParcel = [
                'id' => $parcel->id,
                'pickup_user_id' => $parcel->pickup_user_id,
                'receiver_user_id' => $parcel->receiver_user_id,
                'display_name' => $parcel->display_name,
                'parcel_type' => $parcel->parcel_type,
                'parcel_worth' => $parcel->parcel_worth,
                'parcel_details' => $parcel->parcel_details,
                'submit_by_user' => $parcel->submit_by_user,
                'item_weight' => $parcel->item_weight,
                'type' => null,
                'code' => null,
                'cancelled' => false,
                'status' => null,
                'rider_name' => null,
            ];
        
            if ($pickupUserId == $userId) {
                $responseParcel['type'] = 'Pickup code';
                $responseParcel['code'] = $parcel->acceptedParcels->pickup_code ?? null;
            } elseif ($receiverUserId == $userId) {
                $responseParcel['type'] = 'Receiver code';
                $responseParcel['code'] = $parcel->acceptedParcels->receiver_code ?? null;
            } else {
                $responseParcel['type'] = "You are not the sender nor the receiver";
            }
        
            // Check if parcel is cancelled
            $responseParcel['cancelled'] = $parcel->cancelledParcel ? true : false;
        
            if ($responseParcel['cancelled'] || ($parcel->acceptedParcels && $parcel->acceptedParcels->status === '6')) {
                return null;
            }
        
            // if ($parcel->acceptedParcels) {
                $responseParcel['status'] = $parcel->acceptedParcels->status;
                $responseParcel['rider_name'] = $parcel->acceptedParcels->rider->name ?? null;
                $responseParcel['status'] = getStatusText($responseParcel['status']);
            // } else {
            //     $responseParcel['status'] = "Pending";
            //     $responseParcel['rider_name'] = null;
            // }
        
            return $responseParcel;
        });
        
        $filteredParcels = $filteredParcels->filter(); 
        $filteredParcels = $filteredParcels->values(); 

        
        // $allParcels = $allParcels->reject(function ($parcel) {
        //     return $parcel['cancelled'] || $parcel['status'] === '5';
        // });

        return response()->json(successResponse('Parcels fetched', ['allparcel' =>$filteredParcels]), 200);

   
    } catch (\Exception $e) {
        return response()->json(falseResponse('Failed to process request.', ['error' => $e->getMessage()]), 400);

        // return response()->json(['success' => false, 'message' => 'Failed to cancel parcel.', 'data' => ['error' => $e->getMessage()]], 500);
    }


    }



    public function trackParcel(ParcelSummaryRequest $r){
    
        try {
            // $getParcel = ParcelRequest::with([
            //     'acceptedParcels' => function ($query) {
            //         $query->select('id', 'rider_user_id', 'parcel_request_id', 'status');
            //     },
            //     'acceptedParcels.rider' => function ($query) {
            //         $query->select('id', 'name', 'latitude', 'longitude'); 
            //     }
            // ])->findOrFail($r->parcel_id);
            $getParcel = ParcelRequest::with([
                'acceptedParcels' => function ($query) {
                    $query->select('id', 'rider_user_id', 'parcel_request_id', 'status');
                },
                'acceptedParcels.rider' => function ($query) {
                    $query->select('id', 'name', 'latitude', 'longitude');
                    // Include the profile relation from the Rider model
                    $query->with(['profile' => function ($query) {
                        // Select only the profile_photo field
                        $query->select('rider_id','profile_photo');
                    }]);
                }
            ])->findOrFail($r->parcel_id);
            $acceptParcel = $getParcel->acceptedParcels;

            if (!$acceptParcel) {
                return response()->json(falseResponse('Accepted parcels not found for the given parcel request id.', []), 401);
                // throw new \Exception('Accepted parcels not found for the given parcel request id.', 401);


                // return response()->json(['success' => false, 'message' => 'Accepted parcels not found for the given parcel request id', 'data' => []], 404);
            }
        
            $rider = $acceptParcel->rider;
            $riderRating = calRiderRating($rider->id);
            $getParcel->acceptedParcels->rider->rating = $riderRating;
            return response()->json(successResponse('Parcels fetched.', ['parcel' => $getParcel]), 200);
            // return response()->json(['success' => true, 'message' => 'Parcels fetched', 'data' => ['parcel' => $getParcel]], 200);
        
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            Log::channel('version1')->error("Error in track parcel: " . $e->getMessage());
            return response()->json(falseResponse('Parcel not found.', ['error' => $e->getMessage()]), 404);
            // return response()->json(['success' => false, 'message' => 'Parcel not found', 'data' => []], 404);
        
        } catch (\Exception $e) {
            Log::channel('version1')->error("Error in track parcel: " . $e->getMessage());
            return response()->json(falseResponse('An error occurred.', ['error' => $e->getMessage()]), 500);

            // return response()->json(['success' => false, 'message' => 'An error occurred', 'data' => []], 500);
        }
        

    }


    public function fetchRider(ParcelSummaryRequest $r){
        try{
            $getRider = AcceptParcel::where('parcel_request_id', $r->parcel_id)->first();
            $Rider = Rider::select('id', 'name')->with('profile')->find($getRider->rider_user_id);
            return response()->json(successResponse('Parcels fetched.', ['rider' => $Rider]), 200);


        } catch (\Exception $e) {
            Log::channel('version1')->error("Error in track parcel: " . $e->getMessage());
            return response()->json(falseResponse('An error occurred.', []), 500);

            // return response()->json(['success' => false, 'message' => 'An error occurred', 'data' => []], 500);
        }
    }

    
}
