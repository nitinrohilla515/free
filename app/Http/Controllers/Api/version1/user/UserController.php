<?php

namespace App\Http\Controllers\Api\version1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserNotification;
use App\Models\RiderRating;
use App\Models\AcceptParcel;

use JWTAuth;
use App\Services\AuthService;
use App\Http\Requests\User\UpdateProfileRequest;
use App\Http\Requests\UpdateCoordinateRequest;
use App\Http\Requests\User\SubmitRating;
use Carbon\Carbon;
use App\Http\Requests\Common\UpdateToken;








class UserController extends Controller
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }
    //

     /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile(Request $r) {
       
      
        $getUser = User::select('id', 'name', 'mobile', 'country_id', 'profile_photo', 'fcm_token')->with('countryInfo')
  ->findOrFail($r->user_id);
  return response()->json(successResponse('User profile returned.', [ 'user' => $getUser ]), 200);

        //  return response()->json(['success' => true, 'message' => 'User profile returned', 'data' => ['user' => $getUser]], 200);

    }

 /**
     * Get the  Users list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function allUsers(Request $r) {
       
      
        $allUser = User::select('id', 'name', 'mobile', 'country_id', 'profile_photo')->with('countryInfo')
  ->orderBy('created_at', 'desc')->get();
  return response()->json(successResponse('Users list returned.', [ 'usersList' => $allUser ]), 200);

        //  return response()->json(['success' => true, 'message' => 'Users list returned', 'data' => [ 'usersList' => $allUser]], 200);

    }

       /**
     * Update the rider's profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(UpdateProfileRequest $r)
    {
        try {
            $userId = $r->user_id;
            $getUser = User::findOrFail($userId);
            if($r->has('profile_photo')){  
                $profilePhoto = saveFile($r->file('profile_photo'), 'UserProfiles');
            }
            else{
                $profilePhoto = $getUser->profile_photo;
            }
            $getUser->name = $r->name;
            $getUser->profile_photo = $profilePhoto;
            $getUser->save();
    
          
    
            return response()->json(successResponse('User profile updated successfully.', [ 'user' => $getUser ]), 200);

            // return response()->json(['success' => true, 'message' => 'User profile updated successfully', 'data' => ['user' => $getUser]], 200);
        } catch (\Exception $e) {
            return response()->json(falseResponse('Failed to update profile.', [ 'error' => $e->getMessage() ]), 500);

            // return response()->json(['success' => false, 'message' => 'Failed to update profile.', 'data' => ['error' => $e->getMessage()]], 500);
        }
    }

    public function updateCoordinate(UpdateCoordinateRequest $r)
    {
        try {
            $userId = $r->user_id;
            $getUser = User::findOrFail($userId);
            $getUser->latitude = $r->latitude;
            $getUser->longitude = $r->longitude;
            $getUser->save();
            return response()->json(successResponse('User Coordinates updated successfully.', [ 'user' => $getUser ]), 200);

            return response()->json(['success' => true, 'message' => 'User Coordinates updated successfully', 'data' => ['user' => $getUser]], 200);
        } catch (\Exception $e) {
            return response()->json(falseResponse('Failed to update coordinates.', [ 'error' => $e->getMessage() ]), 500);

            // return response()->json(['success' => false, 'message' => 'Failed to update coordinates.', 'data' => ['error' => $e->getMessage()]], 500);
        }
    }



    public function allNotifications(Request $r)
    {
        $userId = $r->user_id;
        $date = $r->date;
        try{
        $query = UserNotification::where('user_id', $userId);
    
      

        $groupedNotifications = $query->orderBy('created_at', 'desc')->paginate(10)->groupBy(function ($date) {
            $parsedDate = Carbon::parse($date->created_at);
            if ($parsedDate->isToday()) {
                return 'Today';
            } elseif ($parsedDate->isYesterday()) {
                return 'Yesterday';
            } else {
                return $parsedDate->format('F d Y');
            }
        });

        $responseData = [];
foreach ($groupedNotifications as $date => $notifications) {
    $responseData[] = [
        'date' => $date,
        'notifications' => $notifications->map(function ($notification) {
            return [
                'id' => $notification->id,
                'user_id' => $notification->user_id,
                'title' => $notification->title,
                'body' => $notification->body,
                'type' => $notification->type,
                'data' => $notification->data,
                'created_at' => $notification->created_at,
                'updated_at' => $notification->updated_at,
                'deleted_at' => $notification->deleted_at,
            ];
        })->toArray(),
    ];
}
        
    
        // }
    
        return response()->json(successResponse('User notifications fetched.', $responseData), 200);
    } catch (\Exception $e) {
        Log::channel('version1')->error("Error in returning notiofications for user notification parcel: " . $e->getMessage());

        return response()->json(falseResponse('Internal server error ', [ 'error' => $e->getMessage() ]), 500);

        // return response()->json(['success' => false, 'message' => 'Failed to submit rating coordinates.', 'data' => ['error' => $e->getMessage()]], 500);
    }
    }


/**
     * Refresh jwt token for user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken(){
        $token = JWTAuth::parseToken()->refresh();
        return response()->json(successResponse('Token refreshed.', [ 'token' => $token, 'token_type' => 'bearer' ]), 200);

        // return response()->json(['success' => true, 'message' => 'Token refreshed', 'data' => ['token' => $token, 'token_type' => 'bearer']], 200);

    }


    public function submitRating(SubmitRating $r)
    {
        try {
            $userId = $r->user_id;
            // $findAcceptParcel = AcceptParcel::where('parcel_request_id', $r->parcel_id)->first();
            $checkRiderRating = RiderRating::where('parcel_id', $r->parcel_id)->get();
            if($checkRiderRating){
                return response()->json(falseResponse('Rating already submitted', null), 401);

            }
            $userRating = RiderRating::create([
                    
                    'rider_id' => $findAcceptParcel->rider_user_id,
                    'user_id' => $userId,
                    'parcel_id' => $r->parcel_id,
                    'rating' => $r->rating


                  
            ]);
    
          
    
            return response()->json(successResponse('Rating submitted', [ 'userRating' => $userRating ]), 201);

            // return response()->json(['success' => true, 'message' => 'Rating submitted', 'data' => ['userRating' => $userRating]], 201);
        } catch (\Exception $e) {
            return response()->json(falseResponse('Failed to submit rating coordinates', [ 'error' => $e->getMessage() ]), 500);

            // return response()->json(['success' => false, 'message' => 'Failed to submit rating coordinates.', 'data' => ['error' => $e->getMessage()]], 500);
        }
    }

    public function updateFcmToken(UpdateToken $r){
        try{
            $userId = $r->user_id;
            // $userId = 1;
            $user = User::find($userId);
            $user->fcm_token = $r->fcm_token;
            $user->save();

            return response()->json(successResponse('FCM token updated.', []), 200);



        }  catch (\Throwable $e) {
            $response = [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'function' => $e->getTrace()[0]['function'] ?? 'N/A',
            ];
            $message = 'Internal server error';
             // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
            // Log::channel('version1')->error('Failed to create parcel: Unknown error', );
        
        Log::channel('version1')->error($message, $response );
        
        
        
        return response()->json(falseResponse($message, $response), 500);
        }




    }

    public function deleteAccount(Request $r){
        try{
            $userId = $r->user_id;
            // $userId = 1;
           $user = User::find($userId);
           $user->delete();
           return response()->json(successResponse('Account deleted.', []), 200);


        }  catch (\Throwable $e) {
            $response = [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'function' => $e->getTrace()[0]['function'] ?? 'N/A',
            ];
            $message = 'Internal server error';
             // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
            // Log::channel('version1')->error('Failed to create parcel: Unknown error', );
        
        Log::channel('version1')->error($message, $response );
        
        
        
        return response()->json(falseResponse($message, $response), 500);
        }
    }
}
