<?php

namespace App\Http\Controllers\Api\version1\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\SignupSigninRequest;
use App\Http\Requests\ResendOtpRequest;
use App\Http\Requests\GenerateTokenRequest;


use Illuminate\Support\Facades\Log;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Carbon\Carbon;
use App\Services\AuthService;


class AuthController extends Controller
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }
    //
   
public function signUpsignIn(SignupSigninRequest $r) {
  
    try {
        $uid = $r->header('uid');
        if(!$uid){
            return response()->json(falseResponse('UID not found in headers.', []), 401);

        } 
            $isUserVerified = $this->authService->verifyUser($uid);
    
            if (!$isUserVerified) {
                return response()->json(falseResponse('User not verified', []), 401);
            }
        $mobile = $r->input('mobile');
        $existingUser = User::where('mobile', $mobile)->whereNotnull('mobile_verified_at')->first();
        $tokenHash = Hash::make(str::random(10));

        if ($existingUser) {       
            $existingUser->fcm_token = $r->fcm_token;
            $existingUser->device_id = $r->device_id;
            $existingUser->save();


            return response()->json(successResponse('User already exist', ['user' => $existingUser,]), 200);

     
            
        } else {

            $countryId = findCountryId($r->country_code);
             
           $user = User::updateOrcreate([
              'mobile' => $r->mobile,
              'country_id' => $countryId->id
            ],[
                'password' => Hash::make(Str::random(8)),
                'fcm_token' => $r->fcm_token,
                'device_id' => $r->device_id
            ]);


            Log::channel('version1')->info("New user created with mobile: $mobile");

            return response()->json(successResponse('User created', ['user' => $user,]), 200);

        }
    } catch (\Exception $e) {
        Log::channel('version1')->error("Error in signUpsignIn: " . $e->getMessage());
        return response()->json(falseResponse('Internal Server Error', ['error' => $e->getMessage()]), 500);

    }
}


public function generateToken(GenerateTokenRequest $r) {
   
    try {
        $uid = $r->header('uid');
        $platform = $r->header('platform');


        if(!$uid){
            return response()->json(falseResponse('UID not found in headers.', []), 401);

    
        }
        $isUserVerified = $this->authService->verifyUser($uid);
    
        if (!$isUserVerified) {
            return response()->json(falseResponse('User not verified', []), 401);

        }
        $token = $this->authService->generateToken($uid, $r->userid, $r->device_id);

        $user = User::find($r->userid);
        $user->mobile_verified_at = Carbon::now();
        $user->fcm_token = $r->fcm_token ? $r->fcm_token : null;
        $user->device_id = $r->device_id ? $r->device_id : null;
        $user->save();
        
            Log::channel('version1')->info("Token generated for user: $user. JWT token created.");

     
            return response()->json(successResponse('Token generated for user', [ 'user' => $user,
            'token' => $token,
            'token_type' => 'bearer',]), 200);

         
            
       
    } catch (\Exception $e) {
        Log::channel('version1')->error("Error in generate Token: " . $e->getMessage());
        return response()->json(falseResponse('Internal Server Error', []), 500);
    }
    }
 
    
 /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->json(successResponse('User successfully signed out', []), 200);

    }
     
}
