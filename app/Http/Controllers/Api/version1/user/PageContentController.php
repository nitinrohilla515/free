<?php

namespace App\Http\Controllers\Api\version1\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserPageContent;


class PageContentController extends Controller
{
    //
    public function PrivacyPolicy(){
        $getContent = UserPageContent::first();
        $headers = [
            'Content-Type' => 'text/html'
        ];
        return response()->json(['success' => false, 'message' => 'Privacy policy page content returned ', 'data' => [ 'pageContent' => $getContent->privacy_policy ]], 200)->withHeaders($headers);;

    }

    public function TermsConditions(){
        $getContent = UserPageContent::first();

        return response()->json(['success' => false, 'message' => 'Privacy policy page content returned ', 'data' => [ 'pageContent' => $getContent->terms_conditions ]], 200);

    }
}
