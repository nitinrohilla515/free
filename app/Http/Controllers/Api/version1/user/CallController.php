<?php

namespace App\Http\Controllers\Api\version1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Rider;
use App\Models\ParcelRequest;
use App\Models\CallHistory;





use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Requests\User\CallRequest;
use App\Services\FirebaseService;
use Illuminate\Support\Facades\Log;




class CallController extends Controller
{
    protected $firebaseService;
    

    public function __construct(FirebaseService $firebaseService)
    { 
        $this->firebaseService = $firebaseService;
    }
    //
    public function callRequest(CallRequest $r){

        try{
         $userId = $r->user_id;
        //  $userId = 1;
          // Create a new message
          if($r->type === 'start'){
    $message = new CallHistory();
    $message->user_id = $userId;
    $message->rider_id = $r->rider;
    $message->parcel_id = $r->parcel_id;

    $message->call_type = 'user_initiated';
    $message->save();
          }

    $findRider = Rider::findOrfail($r->rider);
    $findUser = User::findOrfail($userId);
    $findParcel= ParcelRequest::with('acceptedParcels')->findOrfail($r->parcel_id);



    $status = true;
    $message = 'Call request processed';
   if($r->type === 'end'){
       $status = true;
       $message = 'Call end request processed';
   }




    $title = 'New call request.';
     $body = "call notification";
     $data = [
        'parcel_id' => $r->parcel_id,
        'call_status' => $status,
        'sender_id' => $findRider->id,

        'sender_name' => $findUser->name,
        'sender_name' => $findUser->name,
        'sender_photo' => $findUser->profile_photo,

        'receiver_id' => $findRider->id,

        'receiver_name' => $findRider->name,
        'receiver_photo' => $findRider->profile->profile_photo ?? null,
        'status' => $findParcel->acceptedParcels->status ?? null
        // Add any other data as needed
    ];
    // $data = ['sender' => $findUser, 'receiver' => $findRider, 'parcel_id' => $r->parcel_id, 'call_status' => $status]; // Extra data


    $token = $findRider->fcm_token;

    $this->firebaseService->sendNotification($token, $title, $body, $data);
    return response()->json(successResponse($message, []), 200);


}  catch (\Throwable $e) {
    $response = [
        'message' => $e->getMessage(),
        'file' => $e->getFile(),
        'line' => $e->getLine(),
        'function' => $e->getTrace()[0]['function'] ?? 'N/A',
    ];
    $message = 'Internal server error';
     // return response()->json(successResponse('Failed to cancel parcel.', ['error' => $e->getMessage()]), 400);
    // Log::channel('version1')->error('Failed to create parcel: Unknown error', );

Log::channel('version1')->error($message, $response );



return response()->json(falseResponse($message, $response), 500);
}


    }
}
