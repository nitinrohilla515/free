<?php

namespace App\Http\Controllers\Api\version1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Common\StartChatRequest;
use App\Http\Requests\Common\SendMessageRequest;

use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\AcceptParcel;
use App\Models\ParcelRequest;
use App\Models\CancelledParcel;


use App\Models\User;
use App\Models\Rider;
use Illuminate\Support\Facades\Log;
use App\Services\FirebaseService;
use App\Models\AppConfig;



class CommonController extends Controller
{
    protected $firebaseService;

    public function __construct(FirebaseService $firebaseService)
    { 
        $this->firebaseService = $firebaseService;
    }
    //
    
    public function startChat(StartChatRequest $r)
    {
       
        $parcelRequest = ParcelRequest::findOrFail($r->parcel_id);
        $acceptedParcel = $parcelRequest->acceptedParcels()->first();
        
        // Check if the parcel is cancelled or delivered
        if (!$acceptedParcel || ($parcelRequest->cancelled || $acceptedParcel->status === '6')) {
            return response()->json(falseResponse('Chat is not active.', []), 400);
        }

        // if ($acceptedParcel->is_cancelled) {
        //     return response()->json(['message' => 'This parcel is cancelled. Chat is not active.'], 400);
        // }
    
        // Check if a chat room already exists for the accepted parcel
        $existingChatRoom = Chat::where('accept_parcel_id', $acceptedParcel->id)->first();
    
        if ($existingChatRoom) {
            // If a chat room already exists, return its ID
            return response()->json(successResponse('Chat room existed', ['chat_room_id' => $existingChatRoom->id]), 200);

        } else {
            // If no chat room exists, create a new one
            $chatRoom = new Chat();
            $chatRoom->acceptParcel()->associate($acceptedParcel);
            $chatRoom->save();
    
            // Return success response with new chat room details
            return response()->json(successResponse('Chat room created', ['chat_room_id' => $chatRoom->id]), 201);

        }
    }


    public function sendMessage(SendMessageRequest $r)
{
   
    try{
    $senderId = $r->user_id;
    // $senderId = 2;
    $appType = $r->appType;
    // $appType = 'User';

    $getChatRoom = Chat::find($r->chat_room_id);
    $acceptedParcel = $getChatRoom->acceptParcel()->first();
    $cancelledParcel = CancelledParcel::where('parcel_request_id', $acceptedParcel->parcel_request_id)->first();
    if(!isset($acceptedParcel) || ($acceptedParcel && $acceptedParcel->status === '6')){
        return response()->json(falseResponse('Cannot send message.', []), 500);

    }
    if($cancelledParcel){
        return response()->json(falseResponse('Parcel is cancelled. Cannot send message.', []), 500);

    }
    

    $parcelRequest = $acceptedParcel->parcelRequest;

    if ($acceptedParcel->status <= '3') {
        // For sender type Rider, receiver will be pickup_user_id
        if ($appType === 'Rider') {
            $receiverId = $parcelRequest->pickup_user_id;
            $senderIdIsValid = ($senderId == $acceptedParcel->rider_user_id);
        } else {
            // For sender type User, receiver will be rider_user_id
            $receiverId = $acceptedParcel->rider_user_id;
            $senderIdIsValid = ($senderId == $parcelRequest->pickup_user_id);
        }
    } elseif ($acceptedParcel->status > '3' && $acceptedParcel->status < '6') {
        // For status greater than 3 and less than 6
        // For sender type Rider, receiver will be receiver_user_id
        if ($appType === 'Rider') {
            $receiverId = $parcelRequest->receiver_user_id;
            $senderIdIsValid = ($senderId == $acceptedParcel->rider_user_id);
        } else {
            // For sender type User, receiver will be rider_user_id
            $receiverId = $acceptedParcel->rider_user_id;
            $senderIdIsValid = ($senderId == $parcelRequest->receiver_user_id);
        }
    } else {
        // Handle other cases if needed
    }

    if (!$senderIdIsValid) {
        // Handle case where senderId is not valid for the selected role
        return response()->json(falseResponse('Cannot send message.', []), 500);
    }


    $senderType = 'App\Models\\' . ($appType === 'User' ? 'User' : 'Rider');
    // $receiverId = $appType === 'User' ? $acceptedParcel->rider_user_id : $parcelRequest->pickup_user_id;
    

    // $senderType = User::find($senderId) ? 'App\Models\User' : (Rider::find($senderId) ? 'App\Models\Rider' : null);



    // Create a new message
    $message = new ChatMessage();
    $message->chat_id = $r->chat_room_id;
    $message->sender_id = $senderId;
    $message->sender_type = $senderType;
    $message->content = $r->content;
    $message->save();



    $title = 'New message received.';
     $body = $r->content;
    $data = ['senderId' => $senderId, 'sender' => $appType, 'receiverId' => $receiverId, 'content' => $r->content, 'chat_id' => $message->chat_id, '']; // Extra data
    $notificationType = '4'; // 4 for pick chat notification 

    // $createNotification = createNotification($receiverId, $notificationType, $title, $body, $data);
    if ($appType === 'User') {
        $token = $acceptedParcel->rider->fcm_token;
        $createNotification = createRiderNotification($receiverId, $notificationType, $title, $body, $data);
    } else {
        $token = $parcelRequest->pickupUser->fcm_token;
        $createNotification = createNotification($receiverId, $notificationType, $title, $body, $data);
    }
    $data['notification_id'] = $createNotification->id;
    $data['type'] = $notificationType;

    $this->firebaseService->sendNotification($token, $title, $body, $data);


    // Return success response with message details
    return response()->json(successResponse('Message sent', []), 201);


} catch (\Exception $e) {
    Log::channel('version1')->error("failed to send message : " . $e->getMessage());

    return response()->json(falseResponse('Failed to create send message.', ['error' => $e->getMessage()]), 500);


}
}

public function getMessages($chatId)
{
   try{
    $chatRoom = Chat::findOrFail($chatId);
    $messages = $chatRoom->messages()->get()->map(function ($message) {
        $senderType = $message->sender_type === Rider::class ? 'rider' : 'user';
        $sender = $senderType === 'rider' ? Rider::find($message->sender_id) : User::find($message->sender_id);
    
        return [
            'id' => $message->id,
            'chat_room_id' => $message->chat_id,
            'sender_id' => $message->sender_id,
            'sender_type' => $senderType,
            'sender_name' => $sender->name,
            'content' => $message->content,
            'created_at' => $message->created_at,
            'updated_at' => $message->updated_at,
        ];
    });

    return response()->json(successResponse('Chat fetched successfully', ['messages' => $messages]), 200);

} catch (\Exception $e) {
    Log::channel('version1')->error("failed to process request : " . $e->getMessage());

    return response()->json(falseResponse('Failed to process request.', ['error' => $e->getMessage()]), 500);


}
}


 public function chatList(Request $r){
    //  $appType = 'rider';
     $appType = $r->appType;
     $user_id = $r->user_id;
    //  $appType = 'User';
    //  $user_id = 3;

     try{
    // $activeParcelsQuery = $appType === 'Rider' ? 'pickup_user_id' : 'rider_user_id';
    $activeParcelsQuery = $appType === 'Rider' ? 'rider_user_id' : 'pickup_user_id';



    $chatRooms = Chat::whereHas('acceptParcel.parcelRequest', function ($query) use ($user_id, $activeParcelsQuery) {
        $query->where($activeParcelsQuery, $user_id);
    })
    ->with([
        'acceptParcel.parcelRequest.pickupUser' => function ($query) {
            $query->select('id', 'name', 'profile_photo'); 
        },
        'acceptParcel.rider' => function ($query) {
            $query->select('id', 'name'); 
        },
        'acceptParcel.rider.profile' => function ($query) {
            $query->select('rider_id','profile_photo'); 
        }
    ])->get();


    $formattedChatRooms = $chatRooms->map(function ($chatRoom) use ($appType) {
        $opposite = $appType === 'Rider' ? $chatRoom->acceptParcel->parcelRequest->pickupUser : $chatRoom->acceptParcel->rider;

        return [
            'chat_id' => $chatRoom->id,
            'opposite_id' => $opposite->id,
            'opposite_name' => $opposite->name,
            'opposite_profile_photo' => $opposite->profile->profile_photo ?? null,
            'last_message' => $chatRoom->messages->isEmpty() ? null : [
                'id' => $chatRoom->messages->last()->id,
                'content' => $chatRoom->messages->last()->content,
                'created_at' => $chatRoom->messages->last()->created_at,
            ],
            'parcel_status' => getStatusText($chatRoom->acceptParcel->status),
            'parcel_id' => $chatRoom->acceptParcel->parcel_request_id

        ];
    });

    return response()->json(successResponse('Chat list fetched successfully', ['list' => $formattedChatRooms]), 200);
    
} catch (\Exception $e) {
    Log::channel('version1')->error("failed to process request : " . $e->getMessage());

    return response()->json(falseResponse('Failed to process request.', ['error' => $e->getMessage()]), 500);
}}

public function appConfig(){  
    $getConfigdata = AppConfig::select('key', 'value')->get();
    $formattedData = [];

// Loop through each AppConfig record
foreach ($getConfigdata as $config) {
    // Add key-value pairs to the formatted data array
    $formattedData[$config->key] = $config->value;
}
    return response()->json(successResponse('App config data returned successfully.', $formattedData), 201);
}

}
