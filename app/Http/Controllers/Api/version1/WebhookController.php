<?php

namespace App\Http\Controllers\Api\version1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Models\ParcelRequest;
use App\Models\ParcelLog;
use App\Models\PaymentLog;
use App\Models\Payment;
use App\Services\PaystackService;




class WebhookController extends Controller
{
    //
    protected $paystackService;
    

    public function __construct(PaystackService $paystackService)
    { 
        $this->paystackService = $paystackService;
    }


    public function paystackWebhook(Request $request)
    {
        try{
         if ((! $request->isMethod('post')) || ! $request->header('X_PAYSTACK_SIGNATURE', null)) {
                        Log::channel('version1')->error("processing webhook: ".$request);

            throw new AccessDeniedHttpException("Invalid Request");
        }

        $input = $request->getContent();
        $paystack_key = config('paystackwebhooks.secret', env('PAYSTACK_SECRET_KEY'));
        if ($request->header('X_PAYSTACK_SIGNATURE') !== hash_hmac('sha512', $input, $paystack_key)) {
            Log::channel('version1')->error("access denied: ".$input);

            throw new AccessDeniedHttpException("Access Denied");
        }
        Log::channel('version1')->error("access grant: ".$input);

        $payload = json_decode($input, true);



// Extract the event type from the payload
$event = $payload['event'];

// Handle different event types
switch ($event) {
    case 'charge.success':
        Log::channel('version1')->error("charge success event type: ".$event);


        // Handle successful charge event
        $transactionId = $payload['data']['id'];
        // $refrence = $payload['data']['refrence'];





        // Process the transaction or update your database accordingly
        break;
        case 'refund.pending':
            case 'refund.processed':
                $payloadData = $payload['data'];
                $this->paystackService->handleRefundEvent($payloadData);
                break;
    // Add more cases for other event types as needed
    default:
        // Handle unknown event type
        Log::channel('version1')->error("Unknown event type: ".$event);
        break;
}

return response()->json(successResponse('Webhook processed successfully.', []), 200);

}  catch (\Throwable $e) {
    $response = [
        'message' => $e->getMessage(),
        'file' => $e->getFile(),
        'line' => $e->getLine(),
        'function' => $e->getTrace()[0]['function'] ?? 'N/A',
    ];
    $message = 'webhook error';
    // Log::channel('version1')->error('Failed to create parcel: Unknown error', );

Log::channel('version1')->error($message, $response );



return response()->json(falseResponse($message, $response), 500);


}
                //     return response()->json(['success' => true], 200);


        // try {
        //     if ($r->method() !== 'POST' || !$r->header('x-paystack-signature')) {
        //         abort(403, 'Unauthorized');
        //     }
    
        //     $input = $r->getContent();
        //     Log::channel('version1')->error("Webhook content: ".$input);
    
        //     $paystackSecretKey = env('PAYSTACK_SECRET_KEY');
        //     Log::channel('version1')->error("Webhook header: ".$paystackSecretKey);

        //     define('PAYSTACK_SECRET_KEY', env('PAYSTACK_SECRET_KEY'));

        //     // Log::channel('version1')->error("Webhook header 2: ".$r->header('HTTP_X_PAYSTACK_SIGNATURE'));



    
        //     // Validate event
        //     if ($r->header('x-paystack-signature') !== hash_hmac('sha512', $input, PAYSTACK_SECRET_KEY)) {

        //     // if ($r->header('x-paystack-signature') !== hash_hmac('sha512', $input, $paystackSecretKey)) {
        //         Log::channel('version1')->error("Invalid Paystack Signature");
        //         return response()->json(['error' => 'Invalid Paystack Signature'], 400);
        //     }
            
        //     Log::channel('version1')->error("Paystack Signature: ".json_decode($input));
            
        //     // Process webhook payload
        //     // Your code to process the webhook payload goes here
            
        //     // Return a response
        //     return response()->json(['success' => true], 200);
        // } catch (\Exception $e) {
        //     Log::channel('version1')->error("Error processing webhook: ".$e->getMessage());
        //     return response()->json(['error' => 'Internal Server Error'], 500);
        // }
    }

     
    
}
