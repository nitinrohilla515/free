<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rider;


class RiderController extends Controller
{
    //
    public function list(Request $r)
    {
        //
        if ($r->ajax()) {
         
            return Rider::getTableData($r);
        }

        $dateParams = filterDateParameters($r);
        $params = $dateParams['params'];
        $start = $dateParams['start'];
        $end = $dateParams['end'];
        $params['deleted'] = request()->deleted;


        return view('admin.riders.list', $params);
    }
}
