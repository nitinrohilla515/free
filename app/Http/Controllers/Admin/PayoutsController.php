<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ParcelRequest;


class PayoutsController extends Controller
{
    //
    public function list(Request $r)
    {
        //
        if ($r->ajax()) {
           
            return ParcelRequest::getPayouts($r);
        }

        $dateParams = filterDateParameters($r);
        $params = $dateParams['params'];
        $start = $dateParams['start'];
        $end = $dateParams['end'];



        return view('admin.payouts.list', $params);
    }

}
