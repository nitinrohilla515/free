<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Admin;
use App\Models\User;
use App\Models\Rider;
use App\Models\ParcelRequest;






class DashboardController extends Controller
{
    /**
     * Display a count of the resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        //
            // $params['filterStartDate'] = $rStart = $r->filterStartDate ? $r->filterStartDate : PROJECT_START_DATE;
            // $params['filterEndDate'] = $rEnd = $r->filterEndDate ? $r->filterEndDate : Carbon::now()->format('Y-m-d');
            // $params['label'] = $label = $r->label ? $r->label : 'Lifetime';
    
            // if ($label == "Today" || $label == "Yesterday" || $label == "Last 7 Days" || $label == "Last 30 Days" || $label == "Custom Range") {
            //     $start = dbTimezoneToUTC($rStart . " 00:00:00");
            //     $end = dbTimezoneToUTC($rEnd . " 23:59:59");
            // } else {
            //     $start = $rStart . " 00:00:00";
            //     $end = $rEnd . " 23:59:59";
            // }

            $dateParams = filterDateParameters($r);
            $params = $dateParams['params'];
            $start = $dateParams['start'];
            $end = $dateParams['end'];

    
            $users = User::query();
            $delivery_person = Rider::query();
            $parcels = ParcelRequest::query();
 
            $users = dateFilter($users, $r, $start, $end);
            $delivery_person = dateFilter($delivery_person, $r, $start, $end);
            $parcels = dateFilter($parcels, $r, $start, $end);


            $params['total_users'] = $users->count();
            $params['total_delivery_persons'] = $delivery_person->count();
            $params['total_parcel_request'] = $parcels->count();

    
            return view('admin.dashboard.index', $params);
      
    }

    public function dateFilter($query, $r, $start, $end) {
        if (isset($r->filterStartDate) && isset($r->filterEndDate) && $r->filterStartDate != '' && $r->filterEndDate != '') {
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        return $query;
    }

  }
