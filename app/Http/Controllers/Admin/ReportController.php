<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserRoprt;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $r)
    {
        //
      
            //
            if ($r->ajax()) {
               
                return UserRoprt::getTableData($r);
            }
    
            $dateParams = filterDateParameters($r);
            $params = $dateParams['params'];
            $start = $dateParams['start'];
            $end = $dateParams['end'];
    
    
    
            return view('admin.reports.list', $params);
        
        
    }

    
}
