<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ParcelRequest;
use Carbon\Carbon;


class OrdersController extends Controller
{
    //

    public function list(Request $r)
    {
        //
        if ($r->ajax()) {
           
            return ParcelRequest::getTableData($r);
        }

        $dateParams = filterDateParameters($r);
        $params = $dateParams['params'];
        $start = $dateParams['start'];
        $end = $dateParams['end'];



        return view('admin.orders.list', $params);
    }

    public function detail($id){
        try {
            $params['parcelData'] = $data = ParcelRequest::with(['acceptedParcels', 'acceptedParcels.rider',
            'cancelledParcel', 'orderJourney', 'pickupUser', 'reciverUser', 'submitByUser'])->findOrFail($id);
            $params['statusTexts'] = statusTextArray();
            $params['formatted_date'] = Carbon::parse($data->created_at)->isoFormat('MMM D YYYY, h:mmA');

            $params['payment_method'] = $data->payment_method === 1 ? 'Online' : 'COD';
         


            $params['statustext'] = $data->acceptedParcels ? getStatusText($data->acceptedParcels->status) : 'Pending';
            $params['id'] = $id;
            // dd($params);
            // $params['btnColor'] = $data->acceptedParcels ? getStatusBtnColor($data->acceptedParcels->status) : 'rgba(255, 152, 31, 1)';

            return view('admin.orders.detail', $params);



        } catch (ModelNotFoundException $e) {
            abort(404);
        }
       
     
          
        $params['data'] = $query->orderBy('id', 'desc')->paginate(10);
        return view('admin.orders.detail');
    }
}
