<?php

namespace App\Jobs;

use App\Model\Transactions;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Log;
use DB;
use Exception;

class ProcessEmails implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    private $path;
    private $emaildata;
    private $email;
    private $subject;
    private $reply_to;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($path, $emaildata, $email, $subject, $reply_to) {
        $this->path = $path;
        $this->emaildata = $emaildata;
        $this->subject = $subject;
        $this->email = $email;
        $this->reply_to = $reply_to;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $path = $this->path;
        $emaildata = $this->emaildata;
        $subject = $this->subject;
        $email = $this->email;
        $reply_to = $this->reply_to;

        $email_id = $emaildata['email_slug'];
        $mailBody = email_template($email_id);

        $mail = $mailBody->email_body;
        $body = replace_content($mail, $emaildata);
        $emailbody = array('body' => $body);

        $subject_body = $mailBody->email_subject;
        $sub = replace_content($subject_body, $emaildata);

        //    Log::info('test email = ' . $path . " == " .$email."===" . $reply_to."====".  json_encode($emaildata));
        try {
           $ddd= Mail::send($path, $emailbody, function ($mail) use ($email, $sub, $reply_to) {
                $mail->to($email);
                $mail->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                if ($reply_to) {
                    $mail->replyTo($email);
                }
                $mail->subject($sub);
            });

            Log::info('Email dispatched for admin email: ' . $email );

           
        } catch (Exception $e) {
            Log::info('failed to send email = ' . json_encode($email) . " == " . $e->getMessage());
        }
    }

}
