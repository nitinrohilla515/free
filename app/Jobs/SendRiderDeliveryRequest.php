<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Rider;
use App\Models\ParcelRequest;
use Illuminate\Support\Facades\Redis;
use App\Services\FirebaseService;
use Illuminate\Support\Facades\Log;




class SendRiderDeliveryRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 300;

    private $firebaseService;
    private $parcelRequestId;
    private $radius;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(FirebaseService $firebaseService, $parcelRequestId, $radius)
    {
        $this->firebaseService = $firebaseService;
        $this->parcelRequestId = $parcelRequestId;
        $this->radius = $radius;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try{
        //
        $ParcelRequest = ParcelRequest::findOrFail($this->parcelRequestId);
//         $acceptedParcelCount = $ParcelRequest->acceptedParcels()->count();
// Log::channel('version1')->info('Accepted Parcel Count', ['count' => $acceptedParcelCount]);



        if ($ParcelRequest->acceptedParcels()->count() > 0) {
            Log::channel('version1')->info('count', ['parcel_id' => $ParcelRequest->id, 'count' => $ParcelRequest->acceptedParcels()->count()]);

            return;
        }

        Log::channel('version1')->info('outer', ['parcel_id' => $ParcelRequest->id, 'count' => $ParcelRequest->acceptedParcels()->count()]);
        $pickupLocationLat = $ParcelRequest->coordinate_data['pickup']['lat'];
        $pickupLocationLong = $ParcelRequest->coordinate_data['pickup']['long'];

        $cacheKey = "notified_riders_{$this->parcelRequestId}";

      
        $notifiedRiderIds = cache($cacheKey, []);
     

        $nearestRiders = Rider::where('status', '1')->get();
        // $nearestRiders = Rider::where('status', '1')->whereDoesntHave('acceptedParcels', function ($query) {
        //     $query->whereBetween('status', ['1', '5'])
        //     ->whereDoesntHave('parcelRequest.cancelledParcel');
        // })->get();
            if ($this->radius >= env('MAX_DELIVERY_DISTANCE') ) {
                Log::channel('version1')->info('distance', ['parcel_id' => $ParcelRequest->id, 'count' => $ParcelRequest->acceptedParcels()->count()]);
                return;
            }

          
          

        foreach ($nearestRiders as $rider) {
            if (in_array($rider->id, $notifiedRiderIds)) {

                continue; 
            }
        
            $distance = calculateDistance($pickupLocationLat, $pickupLocationLong, $rider->latitude, $rider->longitude, 'Km');
           

        
            if ($distance <= $this->radius) {

                // Send notification to the rider
                $title = 'New delivery request';
                 $body = 'Parcel delivery request';
                $data = ['notificationRiderId' => $rider->id, 'is_accepted' => 0, 'parcel_data' => $ParcelRequest]; 
                $notificationType = '2'; // 1 for  parcel delivery notification 
                $createNotification = createRiderNotification($rider->id, $notificationType, $title, $body, $data);
                $data['notification_id'] = $createNotification->id;
                $this->firebaseService->sendNotification($rider->fcm_token, $title, $body, $data);

                // $cache = cache([$cacheKey => array_merge($notifiedRiderIds, [$rider->id])], now()->addMinute(5));
                $cache = cache([$cacheKey => array_merge($notifiedRiderIds, [$rider->id])], now()->addMinutes(7));
                // print_r($cache);

                Log::channel('version1')->info('Parcel delivery notification to rider', ['parcel_id' => $ParcelRequest->id, 'rider_id' => $rider->id]);
               
              
            }

        }
        $this->redispatchJobWithIncreasedRadius();

    } catch (Exception $e) {
        Log::channel('version1')->info('failed to send notification = ' . json_encode($e) . " == " . $e->getMessage());
    }




      

    }
    private function redispatchJobWithIncreasedRadius()
    {
        // Increment the radius for the next attempt
        $newRadius = $this->radius + 2;

        // Delay the job by a specific time (e.g., 1 minute for the next attempt)
        $delay = now()->addMinutes(1);

        // Re-dispatch the job with the updated radius after a delay
        Log::channel('version1')->info('redispatched', ['parcel_id' => $this->parcelRequestId, 'newradius' => $newRadius, 'delay' => $delay]);

        dispatch((new self($this->firebaseService, $this->parcelRequestId, $newRadius))->delay($delay));
    }
}

