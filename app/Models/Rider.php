<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;




class Rider extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $guarded = [];



       /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

      /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'id'; // Adjust based on your actual primary key column name
    }

        /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }    

    public function countryInfo()
    {
        return $this->belongsTo(Country::class, 'country_id')->select('id', 'name', 'iso', 'phonecode');
    }

    public function profile()
    {
        return $this->hasOne(Riderprofile::class);
    }

    public function ratings(){
        return $this->hasMany(RiderRating::class);
    }

    public function acceptedParcels() {
        return $this->hasMany(AcceptParcel::class, 'rider_user_id');
    }

    public function getTotalRidesAttribute() {
        return $this->acceptedParcels()->count();
    }

    public function getTotalEarningsAttribute() {
        // return $this->acceptedParcels()->with('parcelRequest')->get();
        return $this->acceptedParcels()->with('parcelRequest')->get()->sum(function ($parcel) {
            return $parcel->parcelRequest->amount;
        });
    
    }

    public function getTotalDistanceAttribute() {
        return $this->acceptedParcels()->with('parcelRequest')->get()->sum(function ($parcel) {
            return $parcel->parcelRequest->distance;
        });
    
    }

    public function getSuccessfulRidesAttribute() {
        // Count the number of accepted parcels with status = 6 (success)
        return $this->acceptedParcels()->where('status', '6')->count();
    }

    public function getCancelledRidesAttribute() {
        // Get the list of parcel IDs from CancelledParcel model
        $cancelledParcelIds = CancelledParcel::pluck('parcel_request_id')->toArray();

        // Count the number of accepted parcels that are canceled
        return $this->acceptedParcels()->whereIn('parcel_request_id', $cancelledParcelIds)->count();
    }

    public function getMonthlyData($year = null, $month = null)
    {
        [$year, $month] = $this->validateYearAndMonth($year, $month);
    
        $monthlyData = $this->acceptedParcels()
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->with('parcelRequest')
            ->get()
            ->reduce(function ($monthlyData, $parcel) {
                $monthlyData['monthlyRides']++;
                $monthlyData['monthlyEarnings'] += $parcel->parcelRequest->amount;
                $monthlyData['monthlyDistance'] += $parcel->parcelRequest->distance;
    
                return $monthlyData;
            }, [
                'monthlyRides' => 0,
                'monthlyEarnings' => 0,
                'monthlyDistance' => 0,
            ]);
    
        $monthlyData['averageRideDistance'] = $this->calculateAverage($monthlyData['monthlyDistance'], $monthlyData['monthlyRides']);
        $monthlyData['averageRideEarnings'] = $this->calculateAverage($monthlyData['monthlyEarnings'], $monthlyData['monthlyRides']);
        $rating = $this->ratings()->whereYear('created_at', $year)
        ->whereMonth('created_at', $month);
        $monthlyData['ratingsCount'] = $rating->count();
        $monthlyData['averageRating'] = $rating->avg('rating');
        // $monthlyData['averageRating'] = calRiderRating($this->id);
    
        $monthlyData['successRides'] = $this->acceptedParcels()
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->where('status', '6')
            ->count();
    
        $cancelledParcelIds = CancelledParcel::whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->pluck('parcel_request_id')
            ->toArray();
    
        $monthlyData['cancelRides'] = $this->acceptedParcels()
            ->whereIn('parcel_request_id', $cancelledParcelIds)
            ->count();
    
        $monthlyData['year'] = $year;
        $monthlyData['month'] = $month;
    
        return $monthlyData;
    }
    
    private function validateYearAndMonth($year, $month)
    {
        if ($year === null || $month === null) {
            return [now()->year, now()->month];
        }
    
        return [$year, $month];
    }
    
    private function calculateAverage($total, $count)
    {
        return $count !== 0 ? $total / $count : 0;
    }




    public static function getTableData($r) {

        $query = $data = self::query();
        if ($r->filled('filterStartDate') && $r->filled('filterEndDate')) {
            $startDate = $r->filterStartDate;
            $endDate = $r->filterEndDate;
    
            // Filter data based on the provided date range
            $query->whereBetween('created_at', [$startDate, $endDate]);
        }
        if ($r->filled('filterStatus')) {
            $filterStatus = $r->filterStatus;
        
            // Use ternary operator to filter data based on filterStatus
            $query->where('status', $filterStatus);

        }
        if (isset($r->deleted) && $r->deleted == 1) {
            $query->onlyTrashed();
        }
        $table = Datatables::of($query)
                ->editColumn('created_at', function ($r) {
                    if ($r->created_at) {
                        return convertUtcToTimezone($r->created_at);
                    }
                    return '--';
                })
                ->editColumn('updated_at', function ($r) {
                    if ($r->updated_at) {
                        return convertUtcToTimezone($r->updated_at);
                    }
                    return '--';
                })->editColumn('name', function ($r) {
                    return $r->name;
                })
                ->editColumn('status', function ($r) {
                    return $r->status == 0 ? 'Inactive' : 'Active';
                })->addColumn('action', function ($q) {
                    $params['is_view'] = 1;
                    $params['model'] = $q;
                    $params['view_route'] = route('admin.partners.list');
                    $params['name'] = $q->name ?? '-';
                    return view('admin.datatable.action', $params)->render();
                })
                ->rawColumns(['action'])
                ->make(true);
        return $table->original;
    }

   
}
