<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\DataTables\DataTables;



class UserRoprt extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function parcelRequest()
    {
        return $this->belongsTo(ParcelRequest::class, 'parcel_id', 'id');
    }

    public function submitByUser()
{
    return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
}

public function reportReason()
{
    return $this->hasOne(ReportReason::class);
}

    public static function getTableData($r) {

        $query = $data = self::query();
        if ($r->filled('filterStartDate') && $r->filled('filterEndDate')) {
            $startDate = $r->filterStartDate;
            $endDate = $r->filterEndDate;
    
            // Filter data based on the provided date range
            $query->whereBetween('created_at', [$startDate, $endDate]);
        }
        if (isset($r->deleted) && $r->deleted == 1) {
            $query->onlyTrashed();
        }
        $table = Datatables::of($query)
        ->addColumn('date', function($r) {
            return $r->created_at ? parseDisplayDateNew(convertUtcToTimezone($r->created_at)) : '--';
        })
        ->addColumn('time', function($r) {
            return $r->created_at ? parseDisplayTime(convertUtcToTimezone($r->created_at)) : '--';
        })->addColumn('name', function ($r) {
                    return $r->submitByUser->name;
                })->addColumn('reason', function ($r) {
                    return $r->reportReason->report_reason;
                })
                ->addColumn('status', function ($r) {
                    if ($r->parcelRequest->cancelledParcel) {
                        return 'Cancelled';
                    }elseif ($r->parcelRequest->acceptedParcels) {
                        return getStatusText($r->parcelRequest->acceptedParcels->status);
        
                    } else {
                        return 'Pending';
                    }
                })
                ->addColumn('action', function ($q) {
                    $params['is_view'] = 1;
                  
                    // $params['name'] = $q->name ?? '-';
                    $params['view_route'] = route('admin.user.detail', ['id' => $q->id]); // Pass the order ID to the detail route
        
                    $params['is_delete'] = 1;
                    $params['model'] = $q;
                    $params['delete_route'] = route('admin.users');
                    $params['name'] = $q->name ?? '-';
                    return view('admin.datatable.action', $params)->render();
                })
                ->rawColumns(['action'])
                ->make(true);
        $table->original['deletedRecord'] = $data->onlyTrashed()->count();
        return $table->original;
    }

}
