<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PaymentLog extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'parcel__log_id',
        'paid_by_user',
        'transaction_id',
        'reference',
        'status',
        'amount',
        'gateway_response',
        'paid_at',
     
    ];
}
