<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\DataTables\DataTables;



class ParcelRequest extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'pickup_user_id',
        'receiver_user_id',
        'display_name',
        'parcel_type',
        'parcel_worth',
        'parcel_details',
        'status',
        'submit_by_user',
        'coordinate_data',
        'amount',
        'item_weight',
        'distance',
        'payment_method'
    ];
    protected $casts = [
        'coordinate_data' => 'json',
    ];

    public function acceptedParcels()
    {
       return $this->hasOne(AcceptParcel::class, 'parcel_request_id')->select('id', 'parcel_request_id', 'rider_user_id', 'pickup_code', 'receiver_code', 'status');
    }

    public function rider()
{
    return $this->belongsTo(Rider::class, 'rider_user_id')->withTrashed()->select('id', 'name');
}

public function pickupUser()
{
    return $this->belongsTo(User::class, 'pickup_user_id')->withTrashed()->select('id', 'name','mobile', 'fcm_token', 'profile_photo');
}

public function reciverUser()
{
    return $this->belongsTo(User::class, 'receiver_user_id')->withTrashed()->select('id', 'name', 'mobile','fcm_token', 'profile_photo');   
}

public function submitByUser()
{
    return $this->belongsTo(User::class, 'submit_by_user')->withTrashed();
}


public function cancelledParcel()
{
    return $this->hasOne(CancelledParcel::class, 'parcel_request_id');
}

public function orderJourney()
{
    return $this->hasOne(OrderJourney::class, 'parcel_id');
}

public function userRating()
{
    return $this->hasOne(UserRating::class, 'parcel_id');
}

public function payment()
{
    return $this->hasOne(Payment::class, 'parcel_id');
}


public static function getTableData($r) {
    $query = $data = self::query();
    if($r->filled('id')){
        $query->where('submit_by_user', $r->id);
    }
    if ($r->filled('filterStartDate') && $r->filled('filterEndDate')) {
        $startDate = $r->filterStartDate;
        $endDate = $r->filterEndDate;

        // Filter data based on the provided date range
        $query->whereBetween('created_at', [$startDate, $endDate]);
    }
    if ($r->filled('filterStatus')) {
        $filterStatus = $r->filterStatus;

        // Handle different cases based on the selected filterStatus value
        switch ($filterStatus) {
            case 1:
                // Return records where cancelledParcel exists
                $query->has('cancelledParcel');
                break;
            case 2:
                // Return records where acceptedParcel is not found
                $query->doesntHave('acceptedParcels');
                break;
            case 6:
                // Return records where acceptedParcels status is 6
                $query->whereHas('acceptedParcels', function ($subquery) {
                    $subquery->where('status', '6');
                });
                break;
            case 3:
                // Return records where acceptedParcel is found and status is between 1 to 5
                $query->whereHas('acceptedParcels', function ($subquery) {
                    $subquery->whereBetween('status', ['1', '5']);
                });
                break;
        }
    }
    if ($r->filled('search.value')) {
        $searchValue = $r->input('search.value');
    
        // Apply search condition to 'id' and 'status' columns
        $query->where(function ($query) use ($searchValue) {
            $query->where('id', $searchValue);
        });
    }
 


    if (isset($r->deleted) && $r->deleted == 1) {
        $query->onlyTrashed();
    }

    $table = Datatables::of($query)
        ->editColumn('created_at', function ($r) {
            return $r->created_at ? convertUtcToTimezone($r->created_at) : '--';
        })
        ->editColumn('updated_at', function ($r) {
            return $r->updated_at ? convertUtcToTimezone($r->updated_at) : '--';
        })
        ->editColumn('submit_by_user', function ($r) {
            // Assuming `submitByuserProfile` is a relation in the `ParcelRequest` model
            return $r->submitByUser->name ?? '--';
        })
        ->addColumn('rider', function ($r) {
            // Assuming `submitByuserProfile` is a relation in the `ParcelRequest` model
            return $r->acceptedParcels->rider->name ?? '--';
        })
        ->addColumn('subtotal', function ($r) {
            return number_format($r->amount, 2) ?? '--';
        })
        ->addColumn('taxes', function ($r) {
            return '--';
        })
        ->addColumn('final_ammount', function ($r) {
            return number_format($r->amount, 2) ?? '--';
        })
        ->editColumn('payment_method', function ($r) {
            return $r->payment_method == 0 ? 'Cash On Delivery' : 'Online';
        })
        ->addColumn('rating', function ($r) {
            return $r->userRating->rating ?? 0;
        })
        ->addColumn('status', function ($r) {
            if ($r->cancelledParcel) {
                return 'Cancelled';
            }elseif ($r->acceptedParcels) {
                return getStatusText($r->acceptedParcels->status);

            } else {
                return 'Pending';
            }
        })
        ->addColumn('action', function ($q) {
            $params['is_view'] = 1;
            $params['model'] = $q;
          
            // $params['name'] = $q->name ?? '-';
            $params['view_route'] = route('admin.orders.detail', ['id' => $q->id]); // Pass the order ID to the detail route

            return view('admin.datatable.action', $params)->render();
        })
        ->rawColumns(['action'])
        ->make(true);

    return $table->original;
}

public static function getPayouts($r) {
    $query = $data = self::query();
   
    if ($r->filled('filterStartDate') && $r->filled('filterEndDate')) {
        $startDate = $r->filterStartDate;
        $endDate = $r->filterEndDate;

        // Filter data based on the provided date range
        $query->whereBetween('created_at', [$startDate, $endDate]);
    }
    if ($r->filled('filterStatus')) {
        $filterStatus = $r->filterStatus;

        // Handle different cases based on the selected filterStatus value
        switch ($filterStatus) {
            case 1:
                // Return records where cancelledParcel exists
                $query->whereHas('cancelledParcel');
                break;
            case 2:
                // Return records where acceptedParcel is not found
                $query->doesntHave('acceptedParcels');
                $query->doesntHave('cancelledParcel');
                break;
                case 3:
                    // Return records where acceptedParcel is found and status is between 1 to 5
                    $query->whereHas('acceptedParcels', function ($subquery) {
                        // $subquery->where('status', '1');
    
                        $subquery->whereBetween('status', ['1', '5']);
                    });
                    break;
            case 6:
                // Return records where acceptedParcels status is 6
                $query->whereHas('acceptedParcels', function ($subquery) {
                    $subquery->where('status', '6');
                });
                break;
         
        }
    }
    if ($r->filled('search.value')) {
        $searchValue = $r->input('search.value');
    
        // Apply search condition to 'id' and 'status' columns
        $query->where(function ($query) use ($searchValue) {
            $query->where('id', $searchValue);
        });
    }
 



    $table = Datatables::of($query)
    // ->addColumn('date', function ($r) {
    //     return $r->created_at ? convertUtcToTimezone($r->created_at) : '--';
    // })
    ->addColumn('date', function($r) {
        return $r->created_at ? parseDisplayDateNew(convertUtcToTimezone($r->created_at)) : '--';
    })
    ->addColumn('time', function($r) {
        return $r->created_at ? parseDisplayTime(convertUtcToTimezone($r->created_at)) : '--';
    })
     ->editColumn('submit_by_user', function ($r) {
            // Assuming `submitByuserProfile` is a relation in the `ParcelRequest` model
            return $r->submitByUser->name ?? '--';
        })
       
        ->addColumn('final_ammount', function ($r) {
            return number_format($r->amount, 2) ?? '--';
        })
        ->editColumn('payment_method', function ($r) {
            return $r->payment_method == 0 ? 'Cash On Delivery' : 'Online';
        })
      
        ->addColumn('status', function ($r) {
            if ($r->cancelledParcel) {
                return 'Cancelled';
            }elseif ($r->acceptedParcels) {
                return getStatusText($r->acceptedParcels->status);

            } else {
                return 'Pending';
            }
        })
        ->addColumn('action', function ($q) {
            $params['is_view'] = 1;
            $params['model'] = $q;
          
            // $params['name'] = $q->name ?? '-';
            $params['view_route'] = route('admin.payouts.list', ['id' => $q->id]); // Pass the order ID to the detail route

            return view('admin.datatable.action', $params)->render();
        })
        ->rawColumns(['action'])
        ->make(true);

    return $table->original;
}



}
