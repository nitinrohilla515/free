<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\DataTables\DataTables;

class ParcelLog extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'pickup_user_id',
        'receiver_user_id',
        'display_name',
        'parcel_type',
        'parcel_worth',
        'parcel_details',
        'status',
        'submit_by_user',
        'coordinate_data',
        'amount',
        'item_weight',
        'distance',
        'payment_method'
    ];
    protected $casts = [
        'coordinate_data' => 'json',
    ];

    public function acceptedParcels()
    {
       return $this->hasOne(AcceptParcel::class, 'parcel_request_id')->select('id', 'parcel_request_id', 'rider_user_id', 'pickup_code', 'receiver_code', 'status');
    }

    public function rider()
{
    return $this->belongsTo(Rider::class, 'rider_user_id')->select('id', 'name');
}

public function pickupUser()
{
    return $this->belongsTo(User::class, 'pickup_user_id')->select('id', 'name','mobile', 'fcm_token', 'profile_photo');
}

public function reciverUser()
{
    return $this->belongsTo(User::class, 'receiver_user_id')->select('id', 'name', 'mobile','fcm_token', 'profile_photo');   
}

public function submitByUser()
{
    return $this->belongsTo(User::class, 'submit_by_user');
}

public function payments()
{
    return $this->hasOne(PaymentLog::class, 'parcel_log_id');
}
}
