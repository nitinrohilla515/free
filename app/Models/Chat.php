<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Chat extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];


    public function acceptParcel()
    {
        return $this->belongsTo(AcceptParcel::class);
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class);
    }
}
