<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AcceptParcel extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'parcel_request_id',
        'rider_user_id',
        'pickup_code',
        'receiver_code',
        'status',
    ];


    public static function getRiderSummary($riderId)
    {
        $allParcels = self::where('rider_user_id', $riderId)
            ->with(['parcelRequest' => function ($query) {
                $query->select('id', 'display_name', 'parcel_type', 'parcel_worth', 'parcel_details', 'coordinate_data', 'amount', 'distance', 'item_weight');
            }])
            ->get();

        $todayParcels = $allParcels->filter(function ($parcel) {
            return $parcel->updated_at->toDateString() === now()->toDateString()
                && $parcel->status === '6';
        });

        $totalDistanceToday = $todayParcels->sum(function ($parcel) {
            return $parcel->parcelRequest->distance ?? 0.00;
        });

        $totalEarningsToday = $todayParcels->sum(function ($parcel) {
            return $parcel->parcelRequest->amount ?? 0;
        });

        $totalRidersToday = $todayParcels->count();

        return [
            'totalDistanceToday' => $totalDistanceToday,
            'totalEarningsToday' => $totalEarningsToday,
            'totalRidersToday' => $totalRidersToday,
        ];
    }


    public function parcelRequest()
{
    return $this->belongsTo(ParcelRequest::class)->select([
        'id', 'pickup_user_id', 'receiver_user_id', 'display_name', 'amount', 'distance', 'parcel_type', 'parcel_worth', 'parcel_details', 'submit_by_user'
    ]);
}

public function rider()
{
    return $this->belongsTo(Rider::class, 'rider_user_id')->withTrashed()->select('id', 'name', 'fcm_token', 'mobile');
}
}
