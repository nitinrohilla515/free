<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Payment extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'parcel_id',
        'paid_by_user',
        'transaction_id',
        'refrence',
        'status',
        'amount',
        'gateway_response',
        'paid_at',
     
    ];

    public function parcel()
{
    return $this->hasOne(ParcelRequest::class, 'id');
}

}
