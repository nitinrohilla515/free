<?php

namespace App\Services;


use Firebase\JWT\JWT as FirebaseJWT;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;
use Exception;
use Kreait\Firebase\Factory;
use Firebase\JWT\Key;
use UnexpectedValueException;


class AuthService
{
    private const TOKEN_EXPIRATION_TIME = 30 * 60 * 60 * 24;

    private $JWT_SECRET_KEY;

    public function __construct()
    {
        $this->JWT_SECRET_KEY = env('JWT_SECRET');
        ;
    }

   /**
     * Verify Firebase user by ID.
     *
     * @param string $firebase_id
     * @return bool
     */
    public function verifyUser($firebase_id)
    {
        return 1;
        $configPath = null;

        // switch ($app_type) {
        //     case 'Android':
        //         $configPath = base_path('firebase-credentials.json');
        //         break;
        //     case 'ios':
        //         $configPath = base_path('firebase-ios.plist'); // Assuming plist file path
        //         break;
        //     default:
        //         return false; // Invalid app type
        // }
        $configPath = base_path('firebase-credentials.json');
    
        if (file_exists($configPath)) {

            // if ($app_type === 'Android') {
            //     $factory = (new Factory)->withServiceAccount($configPath);
            // } else {
              
            //     $configContents = file_get_contents($configPath);

            //     // Parse XML
            //     $xml = simplexml_load_string($configContents);
            //     $json = json_encode($xml);
            //     $configArray = json_decode($json, true);
            //     $apiKey = $configArray['dict']['string'][0];
            //     $projectId = $configArray['dict']['string'][4];
            //     $clientId = null;
    
            //     foreach ($configArray['dict']['string'] as $index => $value) {
            //         if ($value == 'google_app_id' && isset($configArray['dict']['string'][$index + 1])) {
            //             $clientId = $configArray['dict']['string'][$index + 1];
            //             break;
            //         }
            //     }
            //     $factory =  (new Factory)->withServiceAccount([
            //         'type' => 'service_account',
            //         'project_id' => $projectId,
            //         'private_key' => $apiKey,
            //         'client_id' => $clientId,
            //         'client_email' => 'firebase-adminsdk-30g78@piikup-app.iam.gserviceaccount.com', 
            //     ]);            }
            $factory = (new Factory)->withServiceAccount($configPath);
            $firebaseAuth = $factory->createAuth();
    
            try {
                $user = (array) $firebaseAuth->getUser($firebase_id);
    
                return $user['uid'] == $firebase_id;
            } catch (\Kreait\Firebase\Exception\Auth\UserNotFound $e) {
                return false; // User not found, return false
            } catch (\Exception $e) {
              
                return false;
            }
        }
    
        return false; // Firebase config file not found
    }

    public function generateToken($firebase_id, $user_id, $device_id)
    {
        
        $payload = [
            'iat' => time(),
            'iss' => 'PAPAYA',
            'exp' => time() + self::TOKEN_EXPIRATION_TIME,
            'firebase_id' => $firebase_id,
            'user_id' => $user_id,
            'sub' => 'Papaya AUTH',
            'device_id' => $device_id
        ];

        return FirebaseJWT::encode($payload, $this->JWT_SECRET_KEY, 'HS256'); 
    }

    public function verifyToken($token)
    {
        try {

            if (empty($token)) {
                throw new \Exception('Bearer Token is missing');
            }

            // $payload = FirebaseJWT::decode($token, $this->JWT_SECRET_KEY, ['HS256']);
            $decoded = FirebaseJWT::decode($token, new Key($this->JWT_SECRET_KEY, 'HS256'));

            print_r($decoded);

            if (!isset($decoded->firebase_id)) {
                throw new \Exception('JWT ERROR: Missing Firebase ID');
            }

            return ['error' => false, 'firebase_id' => $decoded->firebase_id];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => $e->getMessage(), 'data' => null];
        }
    }


    private function getBearerToken()
    {
        $headers = getallheaders();

        if (isset($headers['Authorization']) && strpos($headers['Authorization'], 'Bearer ') === 0) {
            return substr($headers['Authorization'], 7);
        }

        return null;
    }

}
