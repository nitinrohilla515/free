<?php 

namespace App\Services;

use Paystack;
use Illuminate\Support\Facades\Log;
use App\Jobs\SendRiderDeliveryRequest;
use App\Models\ParcelRequest;
use App\Models\ParcelLog;
use App\Models\PaymentLog;
use App\Models\Payment;
use App\Models\CancelledParcel;

use Illuminate\Support\Facades\Http;




class PaystackService
{
    
   function makeRequest($getParcel, $userId){

    $amount = $getParcel->amount * 100;
    $data = array(
        "amount" => $amount,
        "reference" => Paystack::genTranxRef(),
        "email" => 'user@mail.com',
        "currency" => "GHS",
        "orderID" => $getParcel->id,
        "metadata" => json_encode(['parcelLogId' => $getParcel->id, 'user_id' => $userId ]),
    );
  

    // return Paystack::getAuthorizationUrl()->redirectNow();
                    $authorizationUrl = Paystack::getAuthorizationUrl($data);

    // $authorizationUrl = Paystack::makePaymentRequest($data);
    // print_r($authorizationUrl);
    return $authorizationUrl;
   }


   function handleCallback(){

   $response = Paystack::getPaymentData();
        $parcelLogId = $response['data']['metadata']['parcelLogId'];
        $userId = $response['data']['metadata']['user_id'];
        $paymentData = $response['data'];

        // Convert paid_at to MySQL datetime format
        $paidAtDateTime = date('Y-m-d H:i:s', strtotime($paymentData['paid_at']));

        // Create payment log
        $paymentLog = new PaymentLog;
        $paymentLog->parcel_log_id = $parcelLogId;
        $paymentLog->paid_by_user = $userId;
        $paymentLog->transaction_id = $paymentData['id'];
        $paymentLog->reference = $paymentData['reference'];
        $paymentLog->paid_at = $paidAtDateTime;
        $paymentLog->amount = $paymentData['amount'];
        $paymentLog->gateway_response = $paymentData['gateway_response'];
        $paymentLog->status = $paymentData['status'];
        $paymentLog->save();
     

        // If payment status is success, create payment entry
        if ($response['data']['status'] === "success") {
            $ParcelLog = ParcelLog::find($parcelLogId);
            $ParcelLog->payment_method = '1';
            $ParcelLog->save();
            $parcelRequest = createParcelData($parcelLogId);

            $payment = new Payment;
            $payment->parcel_id = $parcelRequest->id;
            $payment->paid_by_user = $userId;
            $payment->transaction_id = $paymentData['id'];
            $payment->reference = $paymentData['reference'];
            $payment->amount = $paymentData['amount'];
            $payment->gateway_response = $paymentData['gateway_response'];
            $payment->status = $paymentData['status'];
            $payment->paid_at = $paidAtDateTime;
            $payment->save();
            return ['success' => true, 'parcel_id' => $parcelRequest->id];


        } else {
            return ['success' => false];

        }
   }

  //  public function initiateRefund($parcel){
    
  //   $url = "https://api.paystack.co/refund";
  //   $secretKey = env('PAYSTACK_SECRET_KEY');


  // $fields = [
  //   'transaction' => $parcel->payment->transaction_id
  // ];

  // $fields_string = http_build_query($fields);

  // //open connection
  // $ch = curl_init();
  
  // //set the url, number of POST vars, POST data
  // curl_setopt($ch,CURLOPT_URL, $url);
  // curl_setopt($ch,CURLOPT_POST, true);
  // curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
  // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  //   "Authorization: Bearer $secretKey",
  //   "Cache-Control: no-cache",
  // ));
  
  // //So that curl_exec returns the contents of the cURL; rather than echoing it
  // curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
  
  // //execute post
  // $result = curl_exec($ch);
  // return $result;
  // echo $result;
  //  }

  public function initiateRefund($parcel)
{
    $url = "https://api.paystack.co/refund";
    $secretKey = env('PAYSTACK_SECRET_KEY');

    $response = Http::withHeaders([
        "Authorization" => "Bearer $secretKey",
        "Cache-Control" => "no-cache",
    ])->post($url, [
        'transaction' => $parcel->payment->transaction_id
    ]);

    return $response->json();
}


   public function handleRefundEvent($data){

    $payment = Payment::where('reference', $data['transaction_reference'])
        ->first();
   
        Log::channel('version1')->error("upadte refund payment:".$payment);
   
   
   if ($payment) {
   $cancelledParcel = CancelledParcel::where('parcel_request_id', $payment->parcel_id)->first();
   Log::channel('version1')->error("cancelled parcel:".$cancelledParcel);
   
   
   if ($cancelledParcel) {
   $cancelledParcel->refund_status = $data['status'];
   $cancelledParcel->save();
   return true;
   }
   return false;
   }
    }



}    