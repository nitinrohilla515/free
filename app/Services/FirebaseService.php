<?php 

namespace App\Services;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Illuminate\Support\Facades\Log;

class FirebaseService
{
    protected $firebase;


    // public function __construct()
    // {
    //     $this->firebase = (new Factory)
    //         ->withServiceAccount(base_path('firebase-credentials.json'))
    //         ->createMessaging();
    // }

    public function sendNotification($token, $title, $body, $data = [])
    {
        // $message = CloudMessage::new()
        //     ->withNotification(Notification::create($title, $body))
        //     ->withData($data);

        // $this->firebase->getMessaging()->sendTo($token, $message);
        $data = [
            "registration_ids" => [$token],
            "notification" => [
                "title" => $title,
                "body" => $body,  
            ],
            "data" => $data
        ];
        $dataString = json_encode($data);
    
        $headers = [
            'Authorization: key=' . env('FIREBASE_SECRET_KEY'),
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        // $response = curl_exec($ch);
  
        // dd($response);
        $response = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);


if ($httpCode === 200) {
    // Log the success message
    Log::channel('version1')->info('Notification sent successfully', [
        'token' => $token,
        'title' => $title,
        'body' => $body,
        'data' => $data,
    ]);
} else {
    // Log the failure message
    Log::channel('version1')->error('Failed to send notification', [
        'token' => $token,
        'title' => $title,
        'body' => $body,
        'data' => $data,
        'http_code' => $httpCode,
        'response' => $response,
    ]);
}

// if ($httpCode === 200) {
//     print_r("Notification sent successfully!");
// } else {
//     echo "Failed to send notification. HTTP Status Code: $httpCode";
// }



    //     $message = CloudMessage::new()
    //     ->withNotification(Notification::create($title, $body))
    //     ->withData($data);

    // $this->firebase->sendTo($token, $message);
    }
}
