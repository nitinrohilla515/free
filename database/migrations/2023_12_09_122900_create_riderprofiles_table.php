<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;


return new class extends Migration
{
    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riderprofiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rider_id');
            $table->integer('age');
            $table->string('address');
            $table->string('driving_license_no');
            $table->string('driving_license_file');
            $table->string('vehicle_registration_no');
            $table->string('vehicle_registration_file');
            $table->foreign('rider_id')->references('id')->on('riders')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riderprofiles');
    }
};
