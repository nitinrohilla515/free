<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_ratings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rider_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('parcel_id');


            $table->enum('rating', ['1', '2', '3', '4', '5']);
            $table->string('comment')->nullable();

            $table->foreign('rider_id')->references('id')->on('riders')->onDelete('cascade');
             $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
             $table->foreign('parcel_id')->references('id')->on('parcel_requests')->onDelete('cascade');


 
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_ratings');
    }
};
