<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;


return new class extends Migration
{
    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accept_parcels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parcel_request_id');
            $table->unsignedBigInteger('rider_user_id');
            $table->string('pickup_code');
            $table->string('receiver_code');
            $table->enum('status', ['0', '1', '2', '3', '4', '5', '6'])->default('0');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('parcel_request_id')->references('id')->on('parcel_requests')->onDelete('cascade');
            $table->foreign('rider_user_id')->references('id')->on('riders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accept_parcels');
    }
};
