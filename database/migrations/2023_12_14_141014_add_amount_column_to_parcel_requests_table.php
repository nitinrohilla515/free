<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parcel_requests', function (Blueprint $table) {
            //
            $table->text('coordinate_data')->after('parcel_details');
            $table->float('amount', 8, 2)->after('coordinate_data');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parcel_requests', function (Blueprint $table) {
            //
        });
    }
};
