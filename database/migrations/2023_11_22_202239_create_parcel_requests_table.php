<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;


return new class extends Migration
{
    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcel_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pickup_user_id');
            $table->unsignedBigInteger('receiver_user_id');
            $table->string('display_name');
            $table->enum('parcel_type', ['paper', 'glass', 'hard', 'others']);
            $table->decimal('parcel_worth', 10, 2);
            $table->text('parcel_details');
            $table->timestamps();
            $table->softDeletes();

            // Foreign key relationships
            $table->foreign('pickup_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('receiver_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcel_requests');
    }
};
