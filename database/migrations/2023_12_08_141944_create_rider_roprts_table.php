<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;


return new class extends Migration
{
    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_roprts', function (Blueprint $table) {
            $table->id();
           $table->unsignedBigInteger('rider_id');
           $table->unsignedBigInteger('report_reason_id');
            $table->string('report');
            $table->string('image');
            $table->foreign('rider_id')->references('id')->on('riders')->onDelete('cascade');
            $table->foreign('report_reason_id')->references('id')->on('report_reasons')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_roprts');
    }
};
