<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rider_roprts', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('parcel_id')->after('rider_id');

            $table->foreign('parcel_id')->references('id')->on('parcel_requests')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rider_roprts', function (Blueprint $table) {
            //
        });
    }
};
