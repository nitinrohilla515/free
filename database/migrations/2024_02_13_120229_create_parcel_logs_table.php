<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcel_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pickup_user_id');
            $table->unsignedBigInteger('receiver_user_id');
            $table->string('display_name');
            $table->enum('parcel_type', ['paper', 'glass', 'hard', 'others']);
            $table->decimal('parcel_worth', 10, 2);
            $table->text('parcel_details');
            $table->unsignedBigInteger('submit_by_user');
            $table->enum('payment_method', ['0', '1'])->comment('0 => COD 1 => Mobile money');
            $table->double('item_weight')->default(0.00);
            $table->text('coordinate_data');
            $table->float('amount', 8, 2);
            $table->float('distance', 8, 2)->default(0.00);
            $table->boolean('is_accepted')->default(false);


            $table->timestamps();
            $table->softDeletes();

            // Foreign key relationships
            $table->foreign('pickup_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('receiver_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('submit_by_user')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcel_logs');
    }
};
