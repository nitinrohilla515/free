<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_journeys', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parcel_id');
            $table->unsignedBigInteger('rider_id');

            $table->enum('journey_status', ['0', '1', '2', '3', '4', '5', '6']);
            $table->string('data');
             $table->timestamps();
             $table->softDeletes();
             $table->foreign('parcel_id')->references('id')->on('parcel_requests')->onDelete('cascade');
             $table->foreign('rider_id')->references('id')->on('riders')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_journeys');
    }
};
