<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parcel_logs', function (Blueprint $table) {
            //
            $table->boolean('status')->default(false)->after('is_accepted')->comment('True => parcel created');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parcel_logs', function (Blueprint $table) {
            //
        });
    }
};
