<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parcel_id');
            $table->unsignedBigInteger('paid_by_user');

            $table->string('transaction_id');
            $table->string('reference');
            $table->integer('amount');
            $table->string('status');
            $table->string('gateway_response');
            $table->datetime('paid_at');



            $table->timestamps();
            $table->softDeletes();
            $table->foreign('parcel_id')->references('id')->on('parcel_requests')->onDelete('cascade');
            $table->foreign('paid_by_user')->references('id')->on('users')->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
