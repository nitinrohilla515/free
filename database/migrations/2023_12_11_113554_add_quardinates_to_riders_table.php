<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riders', function (Blueprint $table) {
            //
            $table->decimal('latitude', 10, 8)->nullable()->after('fcm_token');
            $table->decimal('longitude', 11, 8)->nullable()->after('latitude');
            $table->dropColumn('token_hash');
            $table->dropColumn('otp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riders', function (Blueprint $table) {
            //
        });
    }
};
