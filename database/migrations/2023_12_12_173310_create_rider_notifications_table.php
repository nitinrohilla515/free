<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;


return new class extends Migration
{
    use SoftDeletes;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rider_id');
             $table->string('title');
             $table->string('body');
             $table->enum('type', ['0', '1', '2', '3', '4', '5'])->default('0')->comment('1 => pick drop');
             $table->text('data');
             $table->foreign('rider_id')->references('id')->on('riders')->onDelete('cascade');
 
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_notifications');
    }
};
