<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parcel_requests', function (Blueprint $table) {
            //
            $table->enum('payment_method', ['0', '1'])->comment('0 => COD 1 => Mobile money');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parcel_requests', function (Blueprint $table) {
            //
        });
    }
};
