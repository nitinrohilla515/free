<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        \App\Models\EmailTemplate::create([
            'title' => 'Send Verification Code',
            'slug' => 'send-verification-code-mail',
            'email_subject' => '{{APP_NAME}} Your Verification Code is {{OTP}}',
            'email_body' => '<table cellpadding="0" cellspacing="0" style="width:100%">
            <tbody>
                <tr>
                    <td>
                    <p>Hello {{NAME}},</p>
        
                    <p>You have requested to login to your {{APP_NAME}} admin account! Please use below verification code listed below to login.</p>
        
                    <p>Your verification code is:</p>
        
                    <h2>{{OTP}}</h2>
                    </td>
                </tr>
            </tbody>
        </table>',

        ]);
    }
}
