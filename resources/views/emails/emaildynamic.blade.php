<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{{ env('APP_NAME') }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="http://fonts.cdnfonts.com/css/circular-std-book" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
        crossorigin="anonymous" />
    <style type="text/css">
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: none;
            -webkit-text-resize: 100%;
            text-resize: 100%;
            font-family: 'Lato', sans-serif;
        }

        a {
            outline: none;
            color: #74A79F;
            text-decoration: underline;
        }

        a:hover {
            text-decoration: none !important;
        }

        .nav a:hover {
            text-decoration: underline !important;
        }

        .title a:hover {
            text-decoration: underline !important;
        }

        .title-2 a:hover {
            text-decoration: underline !important;
        }

        .btn:hover {
            opacity: 0.8;
        }

        .btn a:hover {
            text-decoration: none !important;
        }

        .btn {
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }

        table td {
            border-collapse: collapse !important;
        }

        .ExternalClass,
        .ExternalClass a,
        .ExternalClass span,
        .ExternalClass b,
        .ExternalClass br,
        .ExternalClass p,
        .ExternalClass div {
            line-height: inherit;
        }

        @media only screen and (max-width:500px) {
            table[class="flexible"] {
                width: 100% !important;
            }

            table[class="center"] {
                float: none !important;
                margin: 0 auto !important;
            }

            *[class="hide"] {
                display: none !important;
                width: 0 !important;
                height: 0 !important;
                padding: 0 !important;
                font-size: 0 !important;
                line-height: 0 !important;
            }

            td[class="img-flex"] img {
                width: 100% !important;
                height: auto !important;
            }

            td[class="aligncenter"] {
                text-align: center !important;
            }

            th[class="flex"] {
                display: block !important;
                width: 100% !important;
            }

            td[class="wrapper"] {
                padding: 0 !important;
            }

            td[class="holder"] {
                padding: 30px 15px 20px !important;
            }

            td[class="nav"] {
                padding: 20px 0 0 !important;
                text-align: center !important;
            }

            td[class="h-auto"] {
                height: auto !important;
            }

            td[class="description"] {
                padding: 30px 20px !important;
            }

            td[class="i-120"] img {
                width: 120px !important;
                height: auto !important;
            }

            td[class="footer"] {
                padding: 5px 20px 20px !important;
            }

            td[class="footer"] td[class="aligncenter"] {
                line-height: 25px !important;
                padding: 20px 0 0 !important;
            }

            tr[class="table-holder"] {
                display: table !important;
                width: 100% !important;
            }

            th[class="thead"] {
                display: table-header-group !important;
                width: 100% !important;
            }

            th[class="tfoot"] {
                display: table-footer-group !important;
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="width: 100%;margin:0; padding:0;" id="target">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td class="wrapper" style="padding:0 0;">
                <!-- module 1 -->
                <table width="100%" cellpadding="0" cellspacing="0" style="background-color: #f8f9fa;padding: 10px">
                    <tr>
                        <td>
                            <table class="flexible" width="600" align="center" style="margin:10px auto;"
                                cellpadding="0" cellspacing="0">
                                <tr>
                                    <td
                                        style="padding:0;background-color: #F1F1F1;box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;border-radius: 5px;">
                                        <table width="100%" cellpadding="0" cellspacing="0"
                                            style="background-color: #f1f1f1" align="center">
                                            <tr>
                                                <th width="113" align="center" style="padding:0;">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="line-height:0;padding: 0 0;text-align: center;">
                                                          
                                                                <div
                                                                    style="border-bottom: 1px solid rgba(255,255,255,.2);padding: 30px 0 10px;">
                                                                    <a href="#" target="_blank"
                                                                        style="text-decoration:none;display: inline-block;">
                          
                             
                             
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                        </table>
                                        <table align="center" cellpadding="0" cellspacing="0" class="flexible"
                                            style="margin:0 auto;word-break: break-word;" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#ffffff" class="holder" data-bgcolor="bg-block"
                                                        style="vertical-align: top;">

                                                        <table cellpadding="0" cellspacing="0"
                                                            style="min-height: 240px;background:#f1f1f1; color:#000000; width:100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="vertical-align:top">
                                                                        <table cellpadding="0" cellspacing="0"
                                                                            style="width:100%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table align="center"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0"
                                                                                            style="padding:15px 30px; width:600px">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="background-color:#ffffff; padding:20px; vertical-align:top">
                                                                                                        @if (isset($body))
                                                                                                            {!! html_entity_decode($body) !!}
                                                                                                        @endif
                                                                                                        <table cellpadding="0" cellspacing="0" style="width:100%;margin-top: 50px;">
                                                                                                            <tr>
                                                                                                                <td align="left" class="title" data-color="title"
                                                                                                                    data-link-color="link title color"
                                                                                                                    data-link-style="text-decoration:none; color:#000000;"
                                                                                                                    data-max="45" data-min="25"
                                                                                                                    data-size="size title"
                                                                                                                    style="font:16px/19px 'Lato', sans-serif; color:#000000;">
                                                                                                                    <span
                                                                                                                        style="margin-bottom: 5px; display: inline-block;">Thank You &amp; Best Regards,</span><br />
                                                                                                                    <strong><span
                                                                                                                            style="color: #216261;font-family: 'Circular Std Bold', sans-serif;">Team
                                                                                                                            {{ env('APP_NAME') }}</span></strong>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>


                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="flexible" width="100%" align="center"
                                            style="margin:0 auto;background-color: ##f1f1f1;padding-bottom: 30px;" cellpadding="0"
                                            cellspacing="0">
                                            <tr>
                                                <td style="line-height:0;padding: 0 30px;text-align: center;">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td data-color="text" data-link-color="link text color"
                                                                data-link-style="text-decoration:underline; color:#000000;"
                                                                class="aligncenter"
                                                                style="font:13px/15px 'Lato', sans-serif; color:#fff; padding:0 0;text-align: center;">
                                                                <div style="background-color: #BCC482; padding: 20px 0;">
                                                                    © Copyright {{ date('Y') }} All Rights Reserved.
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- module 2 -->

            </td>
        </tr>
    </table>
</body>

</html>
