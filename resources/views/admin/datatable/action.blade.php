<div class="d-flex align-items-center justify-content-center">
    @if(!isset(request()->deleted))
        @if(isset($is_view) && $is_view && $view_route)
            <a href="{{ $view_route }}" target="_blank"class="btn btn-primary actionBtn"
               title="View detail"><i class="fas fa-eye"></i></a>
        @endif
        @if(isset($is_edit) && $is_edit)
            <a href="javascript:;" class="btn btn-secondary actionBtn editRow mx-1" title="Edit"
               data-id="{{ $model->id }}"><i
                    class="fas fa-edit"></i></a>
        @endif
        @if(isset($is_delete) && $is_delete && isset($delete_route))
            <a href="javascript:;" class="btn btn-danger table-dbtn actionBtn deleteRow mx-1" title="Delete"
               data-id="{{ $model->id }}" data-type="1" data-url="{{ $delete_route }}" data-name="{{ $name }}"><i
                    class="fas fa-trash"></i></a>
        @endif
    @else
        <a href="javascript:;" class="btn btn-danger table-rbtn actionBtn deleteRow mx-1"
           data-id="{{ $model->id }}" data-type="2" data-url="{{ $delete_route }}" data-name="{{ $name }}"
           title="Restore"><i class="fas fa-undo"></i></a>
{{--        <a href="javascript:;" class="btn btn-danger table-dbtn actionBtn deleteRow" title="Delete User"--}}
{{--           data-id="{{ $model->id }}" data-type="3" data-url="{{ $delete_route }}" data-name="{{ $name }}"><i--}}
{{--                class="fas fa-trash"></i></a>--}}
    @endif
</div>
