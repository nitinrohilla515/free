@extends('admin.layout.login-app')
@section('content')
    <div class="container login-container d-flex justify-content-center align-items-center py-4">
        <div class="row justify-content-center w-100">
            <div class="col-lg-5 col-md-6 col-sm-8">
                <div class="text-center">
                    <a href="{{url('/')}}" class="d-inline-block mb-30px">
                        <!-- <img src="{{ url('/') }}/assets/img/logo.svg" alt="{{env('APP_NAME')}}" /> -->
                    </a>
                    <h3 class="ff_blod mb-30px">Admin Login</h3>
                </div>
                @include('admin.errors.javascript_message_error_success')
                @include('admin.errors.message_error_success')
                <form id="loginForm">
                    <div class="form-group">
                        <label>Email Address</label>
                        <div class="position-relative">
                            <input type="text" name="email" class="form-control form-control-lg login-input"
                                   placeholder="Email Address"/>
                            <i class="fas fa-envelope inputIcon"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <div class="position-relative">
                            <input type="password" name="password" id="password" class="form-control form-control-lg login-input"
                                   placeholder="Password"/>
                            <i class="fas fa-lock pass_showhide inputIcon" toggle="#password"></i>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <a href="{{url('admin/forgot-password')}}" class="ff_book text-underline">Forgot your password?</a>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg" id="loginBtn">Log In
                        <img class="r-btn-arrow" src="{{url('/')}}/backend/assets/images/r-btn-arrow.png" alt="Log In"></button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('pagescript')
    <script src="{{ asset('backend/assets/js/login.js') }}" type="text/javascript"></script>
@endsection
