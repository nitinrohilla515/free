<?php

$lister_request_count = \App\Models\User::count();
$lister_count = \App\Models\User::count();
$customer_count = \App\Models\User::count();
?>
<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu custom-scroll">
            <a href="{{ url('admin/dashboard') }}" class="navbar-brand-menu">
                <img src="#" alt="{{env('APP_NAME')}}" style="color:#fff;"/>
            </a>
            <div class="nav">
                <div class="sidebar-devider"></div>
                <a class="nav-link  @if (!in_array($currentRouteName, $routeArray)) collapsed @endif" href="#"
                   data-toggle="collapse" data-target="#collapseLayouts"
                   aria-expanded="@if (in_array($currentRouteName, $routeArray)) true @else false @endif"
                   aria-controls="collapseLayouts">
                    <div class="menu-profile">
                        <img src="{{asset('backend')}}/assets/images/profile.png" class="fitBox" alt="Profile"/>
                    </div>
                    <span class="pro-menu-name">{{ auth()->user()->name ?? '-' }}</span>
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="sidebar-devider"></div>
                <div class="collapse @if (in_array($currentRouteName, $routeArray)) show @endif" id="collapseLayouts"
                     aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="#">Profile</a>
                        <a class="nav-link" href="#">Change Password</a>
                        <a class="nav-link" href="javascript:;" data-toggle="modal"
                           data-target=".logoutModal">Logout</a>
                    </nav>
                </div>
               <h5 class="left-side-heading">Business</h5>
                <a class="nav-link" href="{{ url('admin/dashboard') }}">
                    <div class="ctm-merge">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-tachometer-alt"></i>
                        </div>
                        Dashboard
                    </div>
                </a>

                <a class="nav-link" href="{{ route('admin.orders.list') }}">
                    <div class="ctm-merge">
                        <div class="sb-nav-link-icon">
                        <i class="fas fa-shopping-cart"></i>
                        </div>
                        Orders
                    </div>
                </a>
                <a class="nav-link" href="{{ route('admin.payouts.list') }}">
                    <div class="ctm-merge">
                        <div class="sb-nav-link-icon">
                        <i class="fas fa-hand-holding-usd"></i>
                        </div>
                        Payouts
                    </div>
                </a>
             
                <a class="nav-link" href="{{ route('admin.users') }}">
                    <div class="ctm-merge">
                        <div class="sb-nav-link-icon">
                        <i class="fas fa-users"></i>
                        </div>
                        Customers
                    </div>
                </a>

                <a class="nav-link" href="{{ route('admin.partners.list') }}">
                    <div class="ctm-merge">
                        <div class="sb-nav-link-icon">
                        <i class="fas fa-truck"></i>
                        </div>
                        Delivery Partners
                    </div>
                </a>







                <h5 class="left-side-heading mt-4">Operations</h5>
                <!-- <a class="nav-link" href="jhj">
    <div class="ctm-merge">
        <div class="sb-nav-link-icon">
            <i class="fas fa-exclamation-circle"></i>
        </div>
        Sos
    </div>
</a> -->

<a class="nav-link" href="{{route('admin.reports.list')}}">
    <div class="ctm-merge">
        <div class="sb-nav-link-icon">
            <i class="fas fa-chart-line"></i>
        </div>
        Reports
    </div>
</a>

<a class="nav-link" href="lkl">
    <div class="ctm-merge">
        <div class="sb-nav-link-icon">
            <i class="fas fa-comment-alt"></i>
        </div>
        Feedbacks
    </div>
</a>



<h5 class="left-side-heading mt-4">Control Center</h5>
<a class="nav-link" href="jhj">
    <div class="ctm-merge">
        <div class="sb-nav-link-icon">
            <i class="fas fa-cogs"></i>
        </div>
        Algorithm
    </div>
</a>

<a class="nav-link" href="jh">
    <div class="ctm-merge">
        <div class="sb-nav-link-icon">
            <i class="fas fa-user-shield"></i>
        </div>
        Permission Control
    </div>
</a>

<a class="nav-link" href="lkl">
    <div class="ctm-merge">
        <div class="sb-nav-link-icon">
            <i class="fas fa-user-lock"></i>
        </div>
        User Dashboard Access
    </div>
</a>

<a class="nav-link" href="lkl">
    <div class="ctm-merge">
        <div class="sb-nav-link-icon">
            <i class="fas fa-file-invoice-dollar"></i>
        </div>
        Tax Management
    </div>
</a>


            
             


               


                <!-- <a class="nav-link mb-5" href="#">
                    <div class="ctm-merge">
                        <div class="sb-nav-link-icon"><i class="fas fa-cog"></i></div>
                        Settings
                    </div>

                </a> -->
            </div>
        </div>
    </nav>
</div>
