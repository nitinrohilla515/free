<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layout.css')
</head>
<body class="login-body">

<main>
    @yield('content')
</main>
@include('admin.layout.script')
</body>
</html>
