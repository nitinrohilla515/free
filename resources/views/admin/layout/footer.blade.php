<footer class="py-3 mx-3 bg-light admin-footer mt-auto">
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between small">
            <div class="text-muted">Copyright &copy; {{ env('APP_NAME') }}</div>
            <div>
                <a href="{{ url('/page/privacy-policy') }}" target="_blank">Privacy Policy</a>
                &middot;
                <a href="{{ url('/page/terms-and-conditions') }}" target="_blank">Terms &amp; Conditions</a>
            </div>
        </div>
    </div>
</footer>
