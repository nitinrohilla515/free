<div class="alert alert-success alert_message" role="alert" id="idAlertSuccessMsg" style="display: none">
    <button class="close closeWarning" type="button"><span>×</span></button>
    <strong>Success!</strong> <span id="idScriptSuccessMsg"></span>
</div>
<div class="alert alert-danger alert_message" role="alert" id="idAlertErrorMsg" style="display: none">
    <button class="close closeWarning" type="button"><span>×</span></button>
    <strong>Error!</strong> <span id="idScriptErrorMsg"></span>
</div>
