@extends('admin.layout.admin-app')
@section('content')
<div class="container-fluid">
    <div class="py-4">
        <div class="d-sm-flex justify-content-between mb-3 mc-flex">
            <h2 class="page-title">All Reports (<span id="total_record"></span>)</h2>
            <!-- <div>
                    <div id="reportrange" class="dateFilter">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <label id="dtLabel" class="mb-0"></label> (<span></span>) <i class="fas fa-chevron-down"></i>
                        <input type="hidden" name="filterStartDate"
                               value="{{ $filterStartDate }}" id="filterStartDate">
                        <input type="hidden" name="filterEndDate"
                               value="{{ $filterEndDate }}" id="filterEndDate">
                    </div>
                </div> -->
                <a class="btn btn-primary" id="filterBtn"><i class="bi bi-filter"></i> Filter</a>

        </div>
        
        @include('admin.filter.table')
        <div class="card admin-card mb-4">
            @include('admin.errors.javascript_message_error_success')
            @include('admin.errors.message_error_success')
            <div class="card-body">
                <div class="table-responsive admin-responsive lr_table">
                    <table class="table table-bordered tablesaw tablesaw-stack" data-tablesaw-mode="stack"
                           id="roleTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>

                                <th>Date.</th>
                                <th>Time</th>
                                <th>Order#</th>
                                <th>Name</th>
                                <th>Report </th>
                                <th>Order Status</th>
                                <th>Action</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     
    </div>
</div>
<input type="hidden" name="label" id="label" value="{{ $label }}">

@endsection
@section('pagescript')
<script>
    var table = $('#roleTable');
    $(document).ready(function () {

    // fetch_data(1);

    $('#filterBtn').click(function () {
        $('body').toggleClass("filter-body-open");
        $('.filter-bar').toggleClass("show-filter");
    });
    $('.filter-close').click(function () {
        $('body').toggleClass("filter-body-open");
        $('.filter-bar').toggleClass("show-filter");
    });

        table.DataTable({
            "scrollX": true,
            processing: true,
            serverSide: true,
            "order": [[0, "asc"]],
            "bAutoWidth": false, // Disable the auto width calculation
            ajax: {
                url: globalSiteUrl + "/admin/reports",
                data: function (data) {
                    data.filterStartDate = $('#filterStartDate').val();
                data.filterEndDate = $('#filterEndDate').val();
                data.filterStatus = $('#filterStatus').val();
                }
            },
            columns: [
                {data: 'date', name: 'date', searchable: false},
                {data: 'time', name: 'time', searchable: true},
                {data: 'id', name: 'id', searchable: false},
                {data: 'name', name: 'name', searchable: false},
                {data: 'reason', name: 'reason', searchable: false},
                {data: 'status', name: 'status', searchable: false},
                {data: 'action', name: 'action', sortable: false, searchable: false},
            ],
            "drawCallback": function (settings) {
                $('#total_record').html(settings.json.recordsTotal)

                $('.role_count').html(settings.json.recordsTotal)
            }
        });

    });

    
  

       
    //for Edit Model
  
    var label = $('#label').val();


      



$(function () {
    var start = moment($('#filterStartDate').val());
    var end = moment($('#filterEndDate').val());

    function cb(start, end, label) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#filterStartDate').val(start.format('YYYY-MM-DD'));
        $('#filterEndDate').val(end.format('YYYY-MM-DD'));
        $('#dtLabel').html(label);
        $('#label').val(label);
        console.log(label);
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: globalDate
    }, cb);
    cb(start, end, label);
});

// $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
//     change_parameter();
//     location.reload();
// });


function change_parameter() {
    filterStartDate = $("#filterStartDate").val();
    filterEndDate = $("#filterEndDate").val();
    filterStatus = $("#filterStatus").val();

    label = $("#label").val();
    url_update = '?filterStartDate=' + filterStartDate;
    url_update += '&filterEndDate=' + filterEndDate;
    url_update += '&filterStatus=' + filterStatus;

    url_update += '&label=' + label;
    //window.history.replaceState(null, null, url_update);
    window.history.pushState(null, null, url_update);
}

  
   

   

   
    
</script>
@endsection
