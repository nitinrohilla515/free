@extends('admin.layout.admin-app')
@section('content')
    <div class="container-fluid">
        <div class="py-4">
            <div class="d-sm-flex justify-content-between mb-3">
                <h2 class="page-title">Dashboard</h2>
                <div>
                    <div id="reportrange" class="dateFilter">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <label id="dtLabel" class="mb-0"></label> (<span></span>) <i class="fas fa-chevron-down"></i>
                        <input type="hidden" name="filterStartDate"
                               value="{{ $filterStartDate }}" id="filterStartDate">
                        <input type="hidden" name="filterEndDate"
                               value="{{ $filterEndDate }}" id="filterEndDate">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card card-1 border-0 shadow text-custom mb-4">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="total_count">
                                    <a href="{{ route('admin.users') }}"
                                       class="h4">{{ $total_users }}</a>
                                       <h4 class="h5 mt-3">Pending Orders</h4>

                                </div>
                                <div class="dash-circle">
                                <img src="{{url('/')}}/assets/img/icons/pendingorder.svg" alt="Pending"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-1 border-0 shadow text-custom mb-4">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="total_count">
                                    <a href="{{ route('admin.users') }}" class="h4">{{ $total_delivery_persons }}</a>
                                    <h4 class="h5 mt-3">Active Orders</h4>

                                </div>
                                <div class="dash-circle">
                                <img src="{{url('/')}}/assets/img/icons/orderCart.svg" alt="Active"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card card-1 border-0 shadow text-custom mb-4">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="total_count">
                                    <a href="{{ route('admin.users') }}" class="h4">{{ $total_parcel_request }}</a>
                                    <h4 class="h5 mt-3">Delivered Orders</h4>

                                </div>
                                <div class="dash-circle">
                                    <img src="{{url('/')}}/assets/img/icons/truck.svg" alt="Delivered"/>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card card-1 border-0 shadow text-custom mb-4">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="total_count">
                                    <a href="{{ route('admin.users') }}" class="h4">{{ $total_parcel_request }}</a>
                                    <h4 class="h5 mt-3">Cancelled Orders</h4>

                                </div>
                                <div class="dash-circle">
                                <img src="{{url('/')}}/assets/img/icons/orderCart.svg" alt="Active"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            
             

             
             
              
              
      
             
             
             
               
             
               
               
            </div>

            <div class="row row-padding">
               
            </div>
        </div>
    </div>
    <input type="hidden" name="label" id="label" value="{{ $label }}">
@endsection
@section('pagescript')
    <script>
        var label = $('#label').val();


      



        $(function () {
            var start = moment($('#filterStartDate').val());
            var end = moment($('#filterEndDate').val());

            function cb(start, end, label) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                $('#filterStartDate').val(start.format('YYYY-MM-DD'));
                $('#filterEndDate').val(end.format('YYYY-MM-DD'));
                $('#dtLabel').html(label);
                $('#label').val(label);
                console.log(label);
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: globalDate
            }, cb);
            cb(start, end, label);
        });

        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            change_parameter();
            location.reload();
        });


        function change_parameter() {
            filterStartDate = $("#filterStartDate").val();
            filterEndDate = $("#filterEndDate").val();
            label = $("#label").val();
            url_update = '?filterStartDate=' + filterStartDate;
            url_update += '&filterEndDate=' + filterEndDate;
            url_update += '&label=' + label;
            //window.history.replaceState(null, null, url_update);
            window.history.pushState(null, null, url_update);
        }

       

      

        
    </script>
@endsection
