
<html>
<head>
    <title>Payment Failed</title>
    <style>
    
        .loader {
        
            display: none; 
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        
        .loader .spinner {
          
            border: 4px solid rgba(0, 0, 0, 0.1);
            border-left-color: #333;
            border-radius: 50%;
            width: 40px;
            height: 40px;
            animation: spin 1s linear infinite;
        }

        @keyframes spin {
            to { transform: rotate(360deg); }
        }
    </style>
</head>
<body>
    <div id="loader" class="loader">
      
        <div class="spinner"></div>
    </div>
    
    <div id="actual-content" style="display: none;">
     
        <p>Payment Failed. Please try again later.</p>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            document.getElementById('loader').style.display = 'block';

            setTimeout(function () {
                document.getElementById('loader').style.display = 'none';
                document.getElementById('actual-content').style.display = 'block';
            }, 60000); 
        });
    </script>
</body>
</html>
