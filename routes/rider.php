
<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\version1\Rider\AuthController;
use App\Http\Controllers\Api\version1\Rider\ParcelController;
use App\Http\Controllers\Api\version1\Rider\RiderController;
use App\Http\Controllers\Api\version1\Rider\ReportController;
use App\Http\Controllers\Api\version1\Rider\PageContentController;
use App\Http\Controllers\Api\version1\Rider\EarningController;

use App\Http\Controllers\Api\version1\CommonController;
use App\Http\Controllers\Api\version1\Rider\WithdrawlController;
use App\Http\Controllers\Api\version1\Rider\CallController;
use App\Http\Controllers\Api\version1\Rider\AppConfigController;

Route::post('signUpsignIn', [AuthController::class, 'signUpsignIn']);
Route::post('generateToken', [AuthController::class, 'generateToken']);
Route::group(['prefix'=> 'v1', 'middleware' => ['jwt.verify.rider']], function() {
    Route::get('profile', [RiderController::class, 'profile']);
    Route::post('acceptRejectParcel', [ParcelController::class, 'acceptRejectParcel']);
    Route::get('allParcelRequests', [ParcelController::class, 'allParcelRequests']);
    Route::get('previousRides', [ParcelController::class, 'previousRides']);
    Route::post('orderJourney', [ParcelController::class, 'orderJourney']);
    Route::get('{id}/parcelDetail', [ParcelController::class, 'parcelDetail']);
    Route::get('toggleStatus', [RiderController::class, 'toggleStatus']);
    Route::get('reportReasonList', [ReportController::class, 'reportReasonList']);
    Route::post('saveReport', [ReportController::class, 'saveReport']);
    Route::post('updateProfile', [RiderController::class, 'updateProfile']);
    Route::post('updateCoordinate', [RiderController::class, 'updateCoordinate']);
    Route::post('submitRating', [RiderController::class, 'submitRating']);
    Route::get('logout', [AuthController::class, 'logout']);
    Route::get('refreshToken', [AuthController::class, 'refreshToken']);
    Route::get('termsConditions', [PageContentController::class, 'termsConditions']);
    Route::get('privacyPolicy', [PageContentController::class, 'privacyPolicy']);
    Route::post('chat/start', [CommonController::class, 'startchat']);
    Route::post('chat/send-message', [CommonController::class, 'sendMessage']);
    Route::get('/chat/{chat_room_id}/messages', [CommonController::class, 'getMessages']);
    Route::get('chat/list', [CommonController::class, 'chatList']);
    Route::get('earnings', [EarningController::class, 'earnings']);
    Route::get('rideHistory', [EarningController::class, 'rideHistory']);
    Route::post('addBank', [WithdrawlController::class, 'addBank']);
    Route::get('bankList', [WithdrawlController::class, 'bankList']);
    Route::post('initiateTransfer', [WithdrawlController::class, 'initiateTransfer']);
    Route::post('callRequest', [CallController::class, 'callRequest']);
    Route::get('appConfig', [CommonController::class, 'appConfig']);
    Route::get('allNotifications', [RiderController::class, 'allNotifications']);
    Route::post('updateFcmToken', [RiderController::class, 'updateFcmToken']);
    Route::get('deleteAccount', [RiderController::class, 'deleteAccount']);

});





