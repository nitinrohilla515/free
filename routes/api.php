<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\version1\user\AuthController;
use App\Http\Controllers\Api\version1\user\ParcelController;
use App\Http\Controllers\Api\version1\user\UserController;
use App\Http\Controllers\Api\version1\user\ReportController;
use App\Http\Controllers\Api\version1\user\PageContentController;
use App\Http\Controllers\Api\version1\CommonController;
use App\Http\Controllers\Api\version1\user\CallController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('signUpsignIn', [AuthController::class, 'signUpsignIn']);
Route::post('generateToken', [AuthController::class, 'generateToken']);
Route::group(['prefix' => 'v1', 'middleware' => ['jwt.verify']], function() {
    Route::post('requestParcel', [ParcelController::class, 'store']);
    Route::post('requestParcelSummary', [ParcelController::class, 'requestParcelSummary']);
    Route::post('parcelRequestPayment', [ParcelController::class, 'parcelRequestPayment']);
    Route::post('sendRiderDeliveryNotification', [ParcelController::class, 'sendRiderDeliveryNotification']);
    Route::get('allParcel', [ParcelController::class, 'allParcel']);
    Route::get('allUsers', [UserController::class, 'allUsers']);
    Route::get('logout', [AuthController::class, 'logout']);
    Route::get('userProfile', [UserController::class, 'userProfile']);
    Route::get('refreshToken', [UserController::class, 'refreshToken']);
    Route::post('cancelParcel', [ParcelController::class, 'cancelParcel']);
    Route::get('allUsers', [UserController::class, 'allUsers']);
    Route::post('updateProfile', [UserController::class, 'updateProfile']);
    Route::get('reportReasonList', [ReportController::class, 'reportReasonList']);
    Route::post('saveReport', [ReportController::class, 'saveReport']);
    Route::post('pickupDropNotification', [ParcelController::class, 'pickupDropNotification']);
    Route::post('pickupDropRequest', [ParcelController::class, 'pickupDropRequest']);
    Route::post('updateCoordinate', [UserController::class, 'updateCoordinate']);
    Route::get('allNotifications', [UserController::class, 'allNotifications']);
    Route::post('submitRating', [UserController::class, 'submitRating']);
    Route::get('activeParcel', [ParcelController::class, 'activeParcel']);
    Route::post('trackParcel', [ParcelController::class, 'trackParcel']);
    Route::get('termsConditions', [PageContentController::class, 'termsConditions']);
    Route::get('privacyPolicy', [PageContentController::class, 'privacyPolicy']);
    Route::post('fetchRider', [ParcelController::class, 'fetchRider']);
    Route::post('chat/start', [CommonController::class, 'startchat']);
    Route::post('chat/send-message', [CommonController::class, 'sendMessage']);
    Route::get('/chat/{chat_room_id}/messages', [CommonController::class, 'getMessages']);
    Route::get('chat/list', [CommonController::class, 'chatList']);
    Route::post('callRequest', [CallController::class, 'callRequest']);
    Route::get('appConfig', [CommonController::class, 'appConfig']);
    Route::post('updateFcmToken', [UserController::class, 'updateFcmToken']);
    Route::get('deleteAccount', [UserController::class, 'deleteAccount']);
});