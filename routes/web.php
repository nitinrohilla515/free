<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\OrdersController;
use App\Http\Controllers\Admin\PayoutsController;
use App\Http\Controllers\Admin\RiderController;
use App\Http\Controllers\Admin\FeedbackController;
use App\Http\Controllers\Api\version1\user\ParcelController;
use App\Http\Controllers\Api\version1\WebhookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [LoginController::class, 'login'])->name('admin.login');
Route::get('/country', [LoginController::class, 'country']);

Route::group(['prefix' => 'admin'], function () {

Route::post('login/authenticate', [LoginController::class, 'authenticate'])->name('admin.authenticate');
Route::get('verification/{id?}', [LoginController::class, 'verification'])->name('admin.verification');
Route::post('verify-verification-code', [LoginController::class, 'verifyVerificationCode'])->name('admin.verify_verification_code');
Route::get('/resend-verification-code', [LoginController::class, 'resendVerificationCode'])->name('admin.resend_verification_code');

Route::get('/refresh_csrf', function () {
    return response()->json(csrf_token());
});
});


Route::group(['prefix' => 'admin' , 'middleware' => 'auth:admin'], function () {
    Route::get('/logout', [LoginController::class, 'logout'])->name('admin.logout');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('/users', [UserController::class, 'index'])->name('admin.users');
    Route::get('/user/{id}/detail', [UserController::class, 'detail'])->name('admin.user.detail');
    Route::get('/reportReason', [ReportController::class, 'index'])->name('admin.reportreason');
    Route::post('/saveReportReason', [ReportController::class, 'store'])->name('admin.reason.store');
    Route::get('/orders', [OrdersController::class, 'list'])->name('admin.orders.list');
    Route::get('/order/{id}/detail', [OrdersController::class, 'detail'])->name('admin.orders.detail');
    Route::get('/payouts', [PayoutsController::class, 'list'])->name('admin.payouts.list');
    Route::get('/deliveryPartners', [RiderController::class, 'list'])->name('admin.partners.list');
    Route::get('/reports', [ReportController::class, 'list'])->name('admin.reports.list');
    Route::get('/feedbacks', [FeedbackController::class, 'list'])->name('admin.feebacks.list');

});

Route::get('/payment/callback', [ParcelController::class, 'paymentCallback'])->name('payment.callback');
Route::post('/v1/paystack/webhook', [WebhookController::class, 'paystackWebhook'])->name('paystack.webhook');
Route::get('/payment/success', function () {
    return view('paymentSuccess');
})->name('payment.success');
Route::get('/payment/failure', function () {
    return view('paymentFailure');
})->name('payment.failure');




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
